package app.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Repository;

import app.model.TrabajadorLaboratorio;


@Repository
public class TrabajadorLaboratorioDAO extends GenericDAO{
    public TrabajadorLaboratorio getTrabajadorLaboratorio(String dni) throws SQLException {
        
        TrabajadorLaboratorio t = null;
        String query = "SELECT * FROM TrabajadorLaboratorio WHERE TrabajadorLaboratorioDNI=?";
        
        try(Connection conn = connector.getConnection();){
            t = queryRunner.query(conn, query, new BeanHandler<TrabajadorLaboratorio>(TrabajadorLaboratorio.class), dni); 
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
          
        return t;

    }
}
