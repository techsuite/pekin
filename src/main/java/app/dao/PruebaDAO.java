package app.dao;

import java.sql.SQLException;
import java.sql.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.commons.dbutils.handlers.BeanHandler;
import app.model.response.PCRDetallesResponse;
import app.model.response.AnalisisSangreDetallesResponse;
import app.model.Prueba;

@Repository
public class PruebaDAO extends GenericDAO{
    @Autowired
    PCRDAO pcrDAO;

    public Prueba getPrueba(Integer idPrueba) throws SQLException{
        Prueba p = null;

		String query = "SELECT * FROM Prueba WHERE idPrueba=?";
		
		try(Connection conn = connector.getConnection();){
			p = queryRunner.query(conn, query, 
                new BeanHandler<Prueba>(Prueba.class), idPrueba);
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		return p;
    }

    public PCRDetallesResponse getPCRDetalles(Integer idPrueba) throws SQLException{
        PCRDetallesResponse pcrResp = null;

		String query = 
            "SELECT \"PCR\" as tipoPrueba, PacienteDNI as pacienteDNI, "+
            "TrabajadorLaboratorioDNI as trabLabDNI, nombre as trabLabNombre, "+
            "FechaRealizacion as fechaRealizacion, Fluorescencia as fluorescencia "+
            "FROM Prueba JOIN PCR JOIN Usuario ON Prueba.idPrueba=PCR.idPrueba "+
            "AND TrabajadorLaboratorioDNI=DNI WHERE Prueba.idPrueba=?";
		
		try(Connection conn = connector.getConnection();){
			pcrResp = queryRunner.query(conn, query, 
                new BeanHandler<PCRDetallesResponse>(PCRDetallesResponse.class), idPrueba);
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		return pcrResp;
    }

    public AnalisisSangreDetallesResponse getAnalisisSangreDetalles(Integer idPrueba) throws SQLException{
        AnalisisSangreDetallesResponse anSangResp = null;

		String query = 
            "SELECT \"AnalisisSangre\" as tipoPrueba, PacienteDNI as pacienteDNI, "+
            "TrabajadorLaboratorioDNI as trabLabDNI, nombre as trabLabNombre, "+
            "FechaRealizacion as fechaRealizacion, Electrolitos as electrolitos, "+
            "Glucosa as glucosa, Colesterol as colesterol, "+
            "Trigliceridos as trigliceridos, Bilirrubina as bilirrubina "+
            "FROM Prueba JOIN AnalisisSangre JOIN Usuario "+
            "ON Prueba.idPrueba=AnalisisSangre.idPrueba "+
            "AND TrabajadorLaboratorioDNI=DNI WHERE Prueba.idPrueba=?";
		
		try(Connection conn = connector.getConnection();){
			anSangResp = queryRunner.query(conn, query, 
                new BeanHandler<AnalisisSangreDetallesResponse>(AnalisisSangreDetallesResponse.class), idPrueba);
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		return anSangResp;
    }
}
