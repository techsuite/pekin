package app.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Repository;

import app.model.Consulta;


@Repository
public class ConsultaDAO extends GenericDAO{
    public Consulta getConsulta(int idConsulta) throws SQLException {
        
        Consulta consulta =null;
        String query = "SELECT * FROM Consulta WHERE idConsulta=?";
        
        try(Connection conn = connector.getConnection();){
            consulta = queryRunner.query(conn, query, new BeanHandler<Consulta>(Consulta.class), idConsulta); 
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return consulta;

    }
}
