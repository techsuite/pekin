package app.dao;

import app.model.Tokenizacion;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.stereotype.Repository;

@Repository
public class TokenizacionDAO extends GenericDAO {

  private Tokenizacion getTokenizacion(Object o)
    throws SQLException, IllegalArgumentException {
    //Don't use this method with other objects
    if (
      !(o instanceof String || o instanceof Integer)
    ) throw new IllegalArgumentException(
      "Only String DNI or Integer token allowed"
    );

    boolean isParamDNI = o instanceof String;

    Tokenizacion t = null;
    String query =
      "SELECT * FROM Tokenizacion WHERE " +
      (isParamDNI ? "Usuario_DNI" : "Token") +
      "=?";

    try (Connection conn = connector.getConnection();) {
      t =
        queryRunner.query(
          conn,
          query,
          new BeanHandler<Tokenizacion>(Tokenizacion.class),
          isParamDNI ? (String) o : (Integer) o
        );

      conn.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return t;
  }

  public Integer getToken(String DNI)
    throws SQLException, IllegalArgumentException {
    Tokenizacion t = getTokenizacion(DNI);
    return t == null ? null : t.getToken();
  }

  public String getDNI(Integer token)
    throws SQLException, IllegalArgumentException {
    Tokenizacion t = getTokenizacion(token);
    return t == null ? null : t.getUsuario_DNI();
  }

  public void generateUserToken(String DNI) throws SQLException {
    String qToken = "INSERT INTO `Tokenizacion` (`Usuario_DNI`) VALUES ( ? );";
    try (Connection conn = connector.getConnection()) {
      queryRunner.insert(conn, qToken, new ScalarHandler<>(), DNI);
    }
  }
}
