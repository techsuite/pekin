package app.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Repository;

import app.model.Doctor;


@Repository
public class DoctorDAO extends GenericDAO{
    public Doctor getDoctor(String dni) throws SQLException {
        
        Doctor d = null;
        String query = "SELECT * FROM Doctor WHERE DoctorDNI=?";
        
        try(Connection conn = connector.getConnection();){
            d = queryRunner.query(conn, query, new BeanHandler<Doctor>(Doctor.class), dni); 
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
          
        return d;

    }
}
