package app.dao.connector;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class Connector {
    public static final String DB_URL =
    //"jdbc:mysql://localhost:3306/core?user=root&password=root&serverTimezone=Europe/Madrid";
      "jdbc:mysql://juliocastrodev.duckdns.org:3306/core?user=root&password=techsuite&sessionVariables=sql_mode=''";
     
      //throws SQLException para mocks
      public Connection getConnection() throws SQLException{
        Connection conn = null;
         
        try {
            conn = DriverManager.getConnection(DB_URL);
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
        
        return conn;
    }
      

}
