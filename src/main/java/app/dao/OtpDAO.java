package app.dao;

import app.model.Otp;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.stereotype.Repository;

@Repository
public class OtpDAO extends GenericDAO {

  //getOtp(Integer id) o getOtp(String dni)
  public Otp getOtp(Object o) throws SQLException, IllegalArgumentException {
    if (
      !(o instanceof String || o instanceof Integer)
    ) throw new IllegalArgumentException(
      "Only String DNI or Integer id allowed"
    );

    boolean isParamDNI = o instanceof String;

    Otp otp = null;
    String query =
      "SELECT * FROM OTP WHERE " + (isParamDNI ? "PacienteDNI" : "idOTP") + "= ?";

    try (Connection conn = connector.getConnection()) {
      otp =
        queryRunner.query(
          conn,
          query,
          new BeanHandler<Otp>(Otp.class),
          isParamDNI ? (String) o : (Integer) o
        );
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return otp;
  }

  public void updateOtp(
    Integer id,
    int value,
    Timestamp expiration,
    String type
  ) throws SQLException {
    String query =
      "UPDATE OTP SET Valor = ?, Expiracion = ?, Tipo = ? WHERE idOTP = ?";

    try (Connection conn = connector.getConnection()) {
      queryRunner.update(conn, query, value, expiration, type, id);
    }
  }

  public void insertOtp(String PacienteDNI, int valor, Timestamp expiracion, String type)
    throws SQLException {
    String query =
      "INSERT INTO OTP (Valor, Expiracion, Tipo, PacienteDNI)  VALUES (?, ?, ?, ?)";

    try (Connection conn = connector.getConnection()) {
      queryRunner.insert(conn, query, new ScalarHandler<>(), 
      valor, expiracion, type, PacienteDNI);
    }
  }

  public void removeOtp(Otp otp) throws SQLException {
    String query = "DELETE FROM OTP WHERE idOTP = ?";

    try (Connection conn = connector.getConnection()) {
      queryRunner.update(conn, query, otp.getIdOTP());
    }
  }
}
