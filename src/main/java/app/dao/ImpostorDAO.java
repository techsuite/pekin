package app.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Repository;

import app.model.Impostor;


@Repository
public class ImpostorDAO extends GenericDAO{
    public Impostor getImpostor(String dni) throws SQLException {
        
        Impostor i = null;
        String query = "SELECT * FROM Impostor WHERE ImpostorDNI=?";
        
        try(Connection conn = connector.getConnection();){
            i = queryRunner.query(conn, query, new BeanHandler<Impostor>(Impostor.class), dni); 
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
          
        return i;

    }
}
