package app.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Repository;
import app.model.User;

@Repository
public class UserDAO extends GenericDAO{
	public User getUser(String dni) throws SQLException{
		User user = null;
		String query = "SELECT * FROM Usuario WHERE DNI=?";
		
		try(Connection conn = connector.getConnection();){
			user = queryRunner.query(conn, query, new BeanHandler<User>(User.class), dni);
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		return user;

	}

	public void setPassword(String dni, String password) throws SQLException {
		String query = "UPDATE Usuario SET contrasena=? WHERE DNI=?";

		try (Connection conn = connector.getConnection()) {
			queryRunner.update(conn, query, password, dni);
			conn.close();
		} catch(SQLException e){
			e.printStackTrace();
		}
  	}
}
