package app.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Repository;

import app.model.Paciente;


@Repository
public class PacienteDAO extends GenericDAO{
    public Paciente getPaciente(String dni) throws SQLException {
        
        Paciente p = null;
        String query = "SELECT * FROM Paciente WHERE PacienteDNI=?";
        
        try(Connection conn = connector.getConnection();){
            p = queryRunner.query(conn, query, new BeanHandler<Paciente>(Paciente.class), dni); 
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
          
        return p;

    }
}
