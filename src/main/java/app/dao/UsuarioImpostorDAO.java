package app.dao;

import java.sql.Connection;
import java.sql.SQLException;

import app.model.UsuarioImpostor;
import app.model.response.UsuarioImpostorResponse;

import org.springframework.stereotype.Repository;
import org.apache.commons.dbutils.handlers.BeanHandler;

@Repository
public class UsuarioImpostorDAO extends GenericDAO{
    public UsuarioImpostor getUsuarioImpostor(String impostorDNI, Integer token) throws SQLException{
        UsuarioImpostor usuarioImpostor = null;
        String query = "SELECT * FROM `Usuario-Impostor` WHERE Impostor_ImpostorDNI=? AND Token=?";
        
        try(Connection conn = connector.getConnection();){
            usuarioImpostor = queryRunner.query(
                conn, query, new BeanHandler<UsuarioImpostor>(UsuarioImpostor.class), 
                    impostorDNI, token);
                
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
          
        return usuarioImpostor;
    }

    public UsuarioImpostorResponse getTokenUsuarioSuplantado(UsuarioImpostor u) throws SQLException{
        UsuarioImpostorResponse ur = null;
        String query = "SELECT Tokenizacion.Token as token FROM "+
            "Tokenizacion JOIN "+
            "(SELECT * FROM `Usuario-Impostor` WHERE Token=? AND Impostor_ImpostorDNI=?) T "+
            "ON T.Usuario_DNI=Tokenizacion.Usuario_DNI";

        try(Connection conn = connector.getConnection();){
            ur = queryRunner.query(
                conn, query, new BeanHandler<UsuarioImpostorResponse>(UsuarioImpostorResponse.class), 
                u.getToken(), u.getImpostor_ImpostorDNI()
            );
            System.out.println(ur);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
          
        return ur;
    }
}
