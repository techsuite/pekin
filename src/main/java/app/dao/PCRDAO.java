package app.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Repository;

import app.model.PCR;


@Repository
public class PCRDAO extends GenericDAO{
    public PCR getPCR(Integer idPrueba) throws SQLException {
        
        PCR p = null;
        String query = "SELECT * FROM PCR WHERE idPrueba=?";
        
        try(Connection conn = connector.getConnection();){
            p = queryRunner.query(conn, query, new BeanHandler<PCR>(PCR.class), idPrueba); 
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
          
        return p;

    }
}
