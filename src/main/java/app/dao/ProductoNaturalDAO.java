package app.dao;

import app.model.response.PageMetadata;
import app.model.response.ProductoNaturalResponse;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ProductoNaturalDAO extends GenericDAO{
    public List<ProductoNaturalResponse> getProductoNatural(String nombre, PageMetadata pageData) throws SQLException {
        List<ProductoNaturalResponse> res=null;

        String query ="SELECT ProductoNatural.Nombre, ProductoNatural.idProductoNatural, ProductoNatural.Contraindicaciones, ProductoNatural.Posologia, ProductoNatural.Prospecto, ProductoNatural.Composicion, ANY_VALUE(URL.URL) AS URL " +
                "FROM `ProductoNatural-URL` JOIN ProductoNatural JOIN URL ON URL.idURL = `ProductoNatural-URL`.idURL AND `ProductoNatural-URL`.idProductoNatural = ProductoNatural.idProductoNatural " +
                "WHERE ProductoNatural.Nombre LIKE '%"+nombre+"%' GROUP BY ProductoNatural.Nombre";

        try(Connection conn = connector.getConnection()){
            res=getPage(ProductoNaturalResponse.class,query,pageData);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;

    }
}
