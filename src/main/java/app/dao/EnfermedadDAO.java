package app.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.handlers.AbstractListHandler;
import org.springframework.stereotype.Repository;

import app.model.response.PageMetadata;

@Repository
public class EnfermedadDAO extends GenericDAO {
    public List<String> getPage(String query, PageMetadata pageData, String args) throws SQLException{
  
    List<String> res = null;
    String paginationQuery = query + " " + "LIMIT "+ 
      pageData.getPageSize() * (pageData.getPageNumber()-1) +", "+ pageData.getPageSize();

    AbstractListHandler<String> alh = new AbstractListHandler<String>(){

      @Override
      protected String handleRow(ResultSet rs) throws SQLException {
        return rs.getString("Nombre");
      }
      
    };

    try(Connection conn = connector.getConnection()){
      res = queryRunner.query(conn, paginationQuery, alh, args);
    }catch(SQLException e){
      e.printStackTrace();
    }

    return res;
  }
}
