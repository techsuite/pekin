package app.dao;

import app.dao.connector.Connector;
import app.model.response.PageMetadata;

import org.apache.commons.dbutils.QueryRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import java.lang.Math;

@Repository
public class GenericDAO {

  @Autowired
  protected Connector connector;

  @Autowired
  protected QueryRunner queryRunner;

  public Long getNumPages(String queryFromWhere, Long pageSize, Object... args) throws SQLException{
    Long rows = null;
    String countRowsQuery = "SELECT count(*) " + queryFromWhere;

    try(Connection conn = connector.getConnection()){
      rows = queryRunner.query(conn, countRowsQuery, new ScalarHandler<Long>(), args);
    }catch(SQLException e){
      e.printStackTrace();
    }
  
    return rows != null ? (long) Math.ceil((double)rows/pageSize) : null;
  }

  public boolean isNumPageInRange(PageMetadata pageData){
    return (long)1 <= pageData.getPageNumber() && pageData.getPageNumber() <= pageData.getTotalPages();
  }

  public <T> List<T> getPage(Class<T> type, String query, PageMetadata pageData, 
      Object... args) throws SQLException{
  
    List<T> res = null;
    String paginationQuery = query + " " + "LIMIT "+ 
      pageData.getPageSize() * (pageData.getPageNumber()-1) +", "+ pageData.getPageSize();

    try(Connection conn = connector.getConnection()){
      res = queryRunner.query(conn, paginationQuery, new BeanListHandler<>(type), args);
    }catch(SQLException e){
      e.printStackTrace();
    }

    return res;
  }

}

