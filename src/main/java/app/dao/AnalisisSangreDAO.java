package app.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.stereotype.Repository;

import app.model.AnalisisSangre;


@Repository
public class AnalisisSangreDAO extends GenericDAO{
    public AnalisisSangre getAnalisisSangre(Integer idPrueba) throws SQLException {
        
        AnalisisSangre an = null;
        String query = "SELECT * FROM AnalisisSangre WHERE idPrueba=?";
        
        try(Connection conn = connector.getConnection();){
            an = queryRunner.query(conn, query, 
                    new BeanHandler<AnalisisSangre>(AnalisisSangre.class), idPrueba); 

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
          
        return an;

    }
}
