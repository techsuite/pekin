package app.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Tokenizacion {
    private String Usuario_DNI;
    private Integer Token;
}
