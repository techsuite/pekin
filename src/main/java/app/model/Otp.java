package app.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class Otp {
    private Integer idOTP;
    private String PacienteDNI;
    private int Valor;
    @JsonFormat (shape = JsonFormat.Shape.NUMBER)
    private Timestamp Expiracion;
    private String Tipo;
}
