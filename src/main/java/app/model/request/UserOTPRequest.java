package app.model.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString

public class UserOTPRequest {
    private String dni;
    private Integer otp;
}
