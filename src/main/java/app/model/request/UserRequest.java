package app.model.request;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class UserRequest {
    String correo_electronico;
    String dni;
}
