package app.model.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString

public class UserATokenRequest {
    private String dni;
    private int authToken;
    private String password;
}
