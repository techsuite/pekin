package app.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PCR {
    private Integer idPrueba;
    private Double Fluorescencia;
}
