package app.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.sql.Timestamp;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ConsultaResponse {
    Integer idConsulta;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    Timestamp fechaRealizacion;
    
    String doctorNombre;
}
