package app.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class ConsultaDetallesResponse {
    private String pacienteDNI;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Timestamp fechaRealizacion;
    
    private String observaciones;
    private String doctorNombre;

}
