package app.model.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ProductoNaturalResponse {
    int idProductoNatural;
    String nombre, contraindicaciones,posologia,prospecto,composicion,url;

}
