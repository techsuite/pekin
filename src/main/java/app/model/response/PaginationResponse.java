package app.model.response;

import lombok.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class PaginationResponse <T> {
  private List<T> data;
  private PageMetadata metadata;
}