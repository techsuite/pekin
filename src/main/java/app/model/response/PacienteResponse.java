package app.model.response;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PacienteResponse {
    String genero, nombre, dni;
    Integer edad;
}
