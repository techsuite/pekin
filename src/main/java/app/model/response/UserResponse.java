package app.model.response;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserResponse {
    private String dni, nombre, apellidos, genero, correo_electronico;
	private Integer edad;
}
