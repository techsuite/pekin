package app.model.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class MedicamentoResponse {
    private Integer idMedicamento;
    private String Nombre, Contraindicaciones, Posologia, Prospecto, Composicion, URL;
}
