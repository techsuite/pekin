package app.model.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class Email {
    
    private String correo_electronico;

    public String Censurado(){
        char[] censurado= correo_electronico.toCharArray(); //Hago copia del correo
        for(int i=0; i<censurado.length;i++){
            if(!(i==0 || i==(censurado.length/2)|| i==censurado.length-2 ||i==censurado.length-1)){
                censurado[i]='*'; //censuro algunas letras
            }
        }
        return new String(censurado); //devuelvo correo censurado
    }


}

