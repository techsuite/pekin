package app.model.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class EnfermedadResponse {
    private Integer idEnfermedad;
    private String nombre, sintomas, recomendaciones, url;
}
