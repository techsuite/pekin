package app.model.response;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PageMetadata {
    private Long pageSize, pageNumber, totalPages;
}
