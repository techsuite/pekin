package app.model.response;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class PCRDetallesResponse extends PruebaDetallesResponse{
    Double fluorescencia;
}
