package app.model.response;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class RegisterResponse {
    private String correo_electronico;
    private int dni;
}
