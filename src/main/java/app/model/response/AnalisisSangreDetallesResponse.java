package app.model.response;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class AnalisisSangreDetallesResponse extends PruebaDetallesResponse{
    Double electrolitos, glucosa, colesterol, trigliceridos, bilirrubina;
}
