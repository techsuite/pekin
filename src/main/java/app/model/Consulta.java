package app.model;

import java.sql.Timestamp;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Consulta {
    private String PacienteDNI, DoctorDNI, Observaciones;
	private Integer idConsulta;
    private Timestamp FechaRealizacion;
}
