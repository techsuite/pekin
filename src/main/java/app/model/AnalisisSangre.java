package app.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AnalisisSangre {
    private Integer idPrueba;
    private Double Electrolitos, Glucosa, Colesterol, Trigliceridos, Bilirrubina;
}
