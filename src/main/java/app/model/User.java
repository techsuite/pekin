package app.model;

import java.sql.Date;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class User {
	private String DNI, contrasena, nombre, apellidos, genero, correo_electronico;
	private Date fecha_nacimiento;
}