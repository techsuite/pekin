package app.model;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UsuarioImpostor {
    private String Usuario_DNI, Impostor_ImpostorDNI;
    private Integer Token;
}
