package app.model;

import java.sql.Timestamp;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Prueba {
    private String TrabajadorLaboratorioDNI, PacienteDNI;
	private Integer idPrueba;
    private Timestamp FechaRealizacion;
}
