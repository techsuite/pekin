package app.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class Medicamento {
    private Integer idMedicamento;
    private String Nombre, Contraindicaciones, Posologia, Prospecto, Composicion;
}
