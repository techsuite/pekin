package app.model;
import lombok.*;

//De momento ya no se usa
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserLogin {
    private String dni, password;
}
