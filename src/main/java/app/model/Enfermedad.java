package app.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode

public class Enfermedad {
    private Integer idEnfermedad;
    private String Nombre, Sintomas, Recomendaciones;
}
