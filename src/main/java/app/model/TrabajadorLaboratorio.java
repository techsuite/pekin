package app.model;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class TrabajadorLaboratorio {
	private String TrabajadorLaboratorioDNI;
}
