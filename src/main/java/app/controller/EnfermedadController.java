package app.controller;

import app.factory.PageMetadataFactory;
import app.factory.PaginationResponseFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import app.model.response.PaginationResponse;
import app.model.response.EnfermedadResponse;
import app.model.response.PageMetadata;
import app.dao.EnfermedadDAO;
import app.dao.TokenizacionDAO;
import app.service.EnfermedadService;
import app.service.UserService;

import java.util.List;

@RestController
public class EnfermedadController {
	@Autowired
    private EnfermedadDAO enfermedadDAO;
    @Autowired
    private TokenizacionDAO tokDAO;
    @Autowired
    private UserService userService;
    @Autowired
    private EnfermedadService enfermedadService;
    @Autowired
    private PageMetadataFactory pageMetadataFactory;
    @Autowired
    private PaginationResponseFactory paginationResponseFactory;


    @GetMapping("/getEnfermedades/{dniPaciente}/{tokenUsuario}/{tamanoPag}/{pagina}")
    public ResponseEntity<PaginationResponse<String>> getEnfermedades(
			@PathVariable String dniPaciente, @PathVariable Integer tokenUsuario, 
            @PathVariable Long tamanoPag, @PathVariable Long pagina) throws Exception {

        ResponseEntity<PaginationResponse<String>> res = null;

		Long numPages = enfermedadService.getPagesGetEnfermedades(tamanoPag, dniPaciente);
        String observadorDNI = tokDAO.getDNI(tokenUsuario);
            
        //Usuario no tiene enfermedades o el observador no existe
        if(numPages.equals(Long.valueOf(0)) || observadorDNI == null) {
            return ResponseEntity.noContent().build();
        }

        List<String> observadorRoles = userService.getRoles(observadorDNI);

        if(observadorRoles.contains("Doctor") || observadorRoles.contains("TrabajadorLaboratorio") ||
                (observadorRoles.contains("Paciente") && dniPaciente.equals(observadorDNI))){

            PageMetadata metadata = pageMetadataFactory.newPageMetadata(tamanoPag,pagina,numPages);
            
            if(enfermedadDAO.isNumPageInRange(metadata)){
                res = ResponseEntity.ok().body(paginationResponseFactory.newPaginationResponse(
                    enfermedadService.getEnfermedades(metadata, dniPaciente), 
                    metadata)
                );
            }else{
                res = ResponseEntity.badRequest().build();
            }

        }else{
            res = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        return res;
	}

    @GetMapping("/getInfoEnfermedad")
    public ResponseEntity<PaginationResponse<EnfermedadResponse>> getInfoEnfermedad
        (@RequestParam String nombre, @RequestParam Long pageSize, @RequestParam Long pageNumber) throws Exception {
        
        ResponseEntity<PaginationResponse<EnfermedadResponse>> res = null;
        
        if(pageSize <= 0)
            return ResponseEntity.badRequest().build();

        Long numPages = enfermedadDAO.getNumPages("FROM (SELECT em.idEnfermedad, Nombre, Sintomas, Recomendaciones, ANY_VALUE(u.URL) AS URL " +
                "FROM Enfermedad AS em LEFT JOIN `Enfermedad-URL` AS eu ON em.idEnfermedad = eu.idEnfermedad " +
                "LEFT JOIN URL AS u ON u.idURL = eu.idURL WHERE em.Nombre LIKE CONCAT('%',?,'%') GROUP BY em.Nombre) q"
                , pageSize, nombre);
        
        if(numPages.equals(Long.valueOf(0))) {
            return ResponseEntity.noContent().build();
        }

        PageMetadata metadata = pageMetadataFactory.newPageMetadata(pageSize,pageNumber,numPages);
            
            if(enfermedadDAO.isNumPageInRange(metadata)){
                res = ResponseEntity.ok().body(paginationResponseFactory.newPaginationResponse(
                   enfermedadService.getInfoEnfermedad(metadata, nombre),
                    metadata));
            }else{
                res = ResponseEntity.badRequest().build();
            }
        
        return res;
    }
}

