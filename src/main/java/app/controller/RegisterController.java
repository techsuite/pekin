package app.controller;

import java.sql.SQLException;
import java.sql.Timestamp;

import app.dao.TokenizacionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import app.dao.OtpDAO;
import app.dao.UserDAO;
import app.model.Otp;
import app.model.User;
import app.model.response.AuthToken;
import app.model.response.UserAToken;
import app.model.response.UserOTP;
import app.model.response.Email;
import app.model.response.UserEmail;

import app.service.EmailService;
import io.micrometer.core.ipc.http.HttpSender.Response;

@RestController  
public class RegisterController {
    //Especifico servidor http que me devuelve cosas.
    @Autowired // Inyectar el objeto que se conecta aumtomaticamente (con ese objto puedo interactuar con la BD)
	UserDAO userDAO; 
    @Autowired
    OtpDAO otpDAO;

    @Autowired
    EmailService emailService;

    @Autowired
    TokenizacionDAO tokenizacionDAO;

    @PostMapping("/register/generateAuthToken")
    public ResponseEntity<AuthToken> generateAuthToken(@RequestBody UserOTP userOtp) throws IllegalArgumentException, SQLException{
        if(userOtp.getDni() == null || userOtp.getOtp() == null)
            return ResponseEntity.badRequest().build();
        
        Otp otp = otpDAO.getOtp(userOtp.getDni());
        if(otp.getValor() == userOtp.getOtp() && otp.getExpiracion().after(new Timestamp(System.currentTimeMillis()))){
            int token = (int)(Math.random()*99999);
            otpDAO.updateOtp(otp.getIdOTP(), token, new Timestamp(System.currentTimeMillis()+600000), "password");
            return ResponseEntity.ok().body(new AuthToken(token));
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/register/setPassword")
    public ResponseEntity<Object> setPassword(@RequestBody UserAToken userAToken) throws IllegalArgumentException, SQLException {
        if(userAToken.getDni() == null || userAToken.getPassword() == null)
            return ResponseEntity.badRequest().build();
        
        Otp otp = otpDAO.getOtp(userAToken.getDni());
        if(!userAToken.getPassword().equals("")
                && otp.getValor() == userAToken.getAuthToken() 
                && otp.getExpiracion().after(new Timestamp(System.currentTimeMillis()))) {
            userDAO.setPassword(userAToken.getDni(), userAToken.getPassword());
            otpDAO.removeOtp(otp);
            tokenizacionDAO.generateUserToken(userAToken.getDni());
            return ResponseEntity.ok().build();
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/register/exists/{dni}") //Ejecuta la func. siguiente cuando (pasa esto), llega una peticion
    public ResponseEntity<Email> exists(@PathVariable String dni) throws Exception { //func. exist que toma variable de url (@pathVariable). Devuelve objeto Email
        User usuario = userDAO.getUser(dni); 

        //Si ya tiene contraseña es que está registrado
        if(usuario==null || !usuario.getContrasena().equals("")){
            return ResponseEntity.noContent().build(); //204
        }
        Email email = new Email(usuario.getCorreo_electronico());
        Email censurado =new Email(email.Censurado()); //Objeto censurado 
        return ResponseEntity.ok().body(censurado); //Devuelvo correo censurado
    }    

    @PostMapping("/register/generateOTP")

    public ResponseEntity<UserEmail> generaOTP(@RequestBody UserEmail user) throws Exception{ //Recibo obj. USerEmail, el cual tiene correo y dni
        if(user.getCorreo_electronico()==null || user.getDni()==null){  //Mirar @valid
            return ResponseEntity.badRequest().build(); //400
        }

        User u = userDAO.getUser(user.getDni());
        
        //Si el correo no es el del usuario o ya tiene contraseña no se registra
        if(!u.getCorreo_electronico().equals(user.getCorreo_electronico())
                || !u.getContrasena().equals(""))
            return ResponseEntity.badRequest().build();

        int otp= (int) (Math.random()*99999); //Entero aleatorio 5 digitos
        emailService.sendEmail(user.getCorreo_electronico(), "El OTP es: " + otp, "Clave OTP registro");
        //Llama servivio Email y da mensaje
        //Saco hora actual y añado 10 min (600000 ms)
        Timestamp expiration = new Timestamp(System.currentTimeMillis()+600000);

        Otp existentOTP = otpDAO.getOtp(user.getDni());

        //Si ya existe en la tabla solo hacer update
        if(existentOTP == null){
            otpDAO.insertOtp(user.getDni(), otp, expiration, "email");
        }else{
            otpDAO.updateOtp(existentOTP.getIdOTP(), otp, expiration, "email");
        }

        return ResponseEntity.ok().build();
    }

}
