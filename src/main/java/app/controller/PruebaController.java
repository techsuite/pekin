package app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import app.model.Prueba;
import app.model.response.PaginationResponse;
import app.model.response.PruebaDetallesResponse;
import app.model.response.PruebaResponse;
import app.model.response.PageMetadata;
import app.dao.PruebaDAO;
import app.dao.TokenizacionDAO;
import app.dao.PCRDAO;
import app.service.PruebaService;
import app.service.UserService;

import java.util.List;

@RestController
public class PruebaController {
	@Autowired
	PruebaDAO pruebaDAO;
    @Autowired
    TokenizacionDAO tokDAO;
	@Autowired
	PCRDAO pcrDAO;
    @Autowired
    UserService userService;
    @Autowired
    PruebaService pruebaService;

    @GetMapping("/getPruebaDetalles/{idPrueba}/{tokenUsuario}")
    public ResponseEntity<PruebaDetallesResponse> getPruebaDetalles(
        @PathVariable Integer idPrueba, @PathVariable Integer tokenUsuario) throws Exception{

            ResponseEntity<PruebaDetallesResponse> res = null;
            
            String observerDNI = tokDAO.getDNI(tokenUsuario);
            Prueba p = pruebaDAO.getPrueba(idPrueba);

            if(p == null || observerDNI == null)
                return ResponseEntity.noContent().build();

            List<String> roles = userService.getRoles(observerDNI);

            if(roles.contains("Doctor") || roles.contains("TrabajadorLaboratorio") ||
                    (roles.contains("Paciente") && observerDNI.equals(p.getPacienteDNI()))){
                    
                res = ResponseEntity.ok(
                    pcrDAO.getPCR(idPrueba) == null
                    ? pruebaDAO.getAnalisisSangreDetalles(idPrueba)
                    : pruebaDAO.getPCRDetalles(idPrueba)
                );
                
            }else{
                res = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            return res;
    }


    @GetMapping("/getPruebas/{dni}/{token}/{fechaFiltro}/"+
        "{tipoPruebaFiltro}/{trabLabNombreFiltro}/{tamanoPagina}/{pagina}")
    public ResponseEntity<PaginationResponse<PruebaResponse>> getPruebas(
			@PathVariable String dni, @PathVariable Integer token, 
            @PathVariable Long fechaFiltro, @PathVariable String trabLabNombreFiltro,
            @PathVariable String tipoPruebaFiltro,
            @PathVariable Long tamanoPagina, @PathVariable Long pagina) throws Exception {

        //Solo se permite llamar con un filtro
        if((!trabLabNombreFiltro.equals("0") && !fechaFiltro.equals(Long.valueOf(0)))
            || (!trabLabNombreFiltro.equals("0") && !tipoPruebaFiltro.equals("0"))
            || (!fechaFiltro.equals(Long.valueOf(0)) && !tipoPruebaFiltro.equals("0")))
                return ResponseEntity.badRequest().build();

        //De momento las pruebas válidas son PCR y AnalisisSangre
        if(!tipoPruebaFiltro.equals("0") && 
            !tipoPruebaFiltro.equals("PCR") && !tipoPruebaFiltro.equals("AnalisisSangre"))
                return ResponseEntity.badRequest().build();

        ResponseEntity<PaginationResponse<PruebaResponse>> res = null;

		Long numPages = pruebaService.getPagesGetPruebas(
            fechaFiltro, trabLabNombreFiltro, tipoPruebaFiltro, tamanoPagina, dni);

        String observadorDNI = tokDAO.getDNI(token);

        //Usuario no tiene pruebas o el observador no existe
        if(numPages.equals(Long.valueOf(0)) || observadorDNI == null)
                return ResponseEntity.noContent().build();

        List<String> observadorRoles = userService.getRoles(observadorDNI);

        if(observadorRoles.contains("Doctor") || observadorRoles.contains("TrabajadorLaboratorio") ||
                (observadorRoles.contains("Paciente") && dni.equals(observadorDNI))){

            PageMetadata metadata = new PageMetadata(tamanoPagina, pagina, numPages);
            
            if(pruebaDAO.isNumPageInRange(metadata)){
                res = ResponseEntity.ok().body(new PaginationResponse<>(
                    pruebaService.getPruebas(
                        fechaFiltro, trabLabNombreFiltro, tipoPruebaFiltro, metadata, dni), 
                    metadata)
                );
            }else{
                res = ResponseEntity.badRequest().build();
            }

        }else{
            res = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        return res;
	}
}
