package app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import app.model.response.PaginationResponse;
import app.model.response.ConsultaResponse;
import app.model.Consulta;
import app.model.Tokenizacion;
import app.model.User;
import app.model.response.ConsultaDetallesResponse;
import app.model.response.PageMetadata;
import app.dao.ConsultaDAO;
import app.dao.TokenizacionDAO;
import app.dao.UserDAO;
import app.service.UserService;
import app.service.ConsultaService;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@RestController
public class ConsultaController {
	@Autowired
	ConsultaDAO consultaDAO;
    @Autowired
    TokenizacionDAO tokDAO;
    @Autowired
    UserService userService;
    @Autowired
    ConsultaService consultaService;
    @Autowired
    UserDAO userDAO;

    @GetMapping("/getConsultas/{dni}/{token}/{fechaFiltro}/{doctorNombreFiltro}/{tamanoPagina}/{pagina}")
    public ResponseEntity<PaginationResponse<ConsultaResponse>> getConsultas(
			@PathVariable String dni, @PathVariable Integer token,
            @PathVariable Long fechaFiltro, @PathVariable String doctorNombreFiltro, 
            @PathVariable Long tamanoPagina, @PathVariable Long pagina) throws Exception {
        
        //No se puede llamar con ambos filtros a la vez
        if(!doctorNombreFiltro.equals("0") && !fechaFiltro.equals(Long.valueOf(0)))
                return ResponseEntity.badRequest().build();
        
        ResponseEntity<PaginationResponse<ConsultaResponse>> res = null;

		Long numPages = consultaService.getPagesGetConsultas(
            fechaFiltro, doctorNombreFiltro, tamanoPagina, dni);

        String observadorDNI = tokDAO.getDNI(token);

        //Usuario no tiene consultas o el observador no existe
        if(numPages.equals(Long.valueOf(0)) || observadorDNI == null)
                return ResponseEntity.noContent().build();

        List<String> observadorRoles = userService.getRoles(observadorDNI);

        if(observadorRoles.contains("Doctor") || observadorRoles.contains("TrabajadorLaboratorio") ||
                (observadorRoles.contains("Paciente") && dni.equals(observadorDNI))){

            PageMetadata metadata = new PageMetadata(tamanoPagina, pagina, numPages);
            
            if(consultaDAO.isNumPageInRange(metadata)){
                res = ResponseEntity.ok().body(new PaginationResponse<>(
                    consultaService.getConsultas(fechaFiltro, doctorNombreFiltro, metadata, dni), 
                    metadata)
                );
            }else{
                res = ResponseEntity.badRequest().build();
            }

        }else{
            res = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        return res;
	}
    //Devuelvo objeto consulta
    @GetMapping("/getConsultaDetalles/{idConsulta}/{tokenUsuario}")
    public ResponseEntity<ConsultaDetallesResponse> getConsultasDetalle(
        @PathVariable int idConsulta, @PathVariable int tokenUsuario) throws Exception{
            Consulta consultaId = consultaDAO.getConsulta(idConsulta);
            String consultaRol = tokDAO.getDNI(tokenUsuario); 

            if(consultaId==null || consultaRol==null){
                return ResponseEntity.noContent().build(); //204
            }

            User usuario = userDAO.getUser(consultaId.getDoctorDNI()); 
            String nombre = usuario.getNombre();
            ConsultaDetallesResponse consulta = new ConsultaDetallesResponse(
                consultaId.getPacienteDNI(), consultaId.getFechaRealizacion(), consultaId.getObservaciones(), nombre);

             
            ResponseEntity<ConsultaDetallesResponse> res = null;
            
            List<String> observadorRoles = userService.getRoles(consultaRol);
            //Con que sea doctor ya puede consultar
            if(observadorRoles.contains("Doctor") || observadorRoles.contains("TrabajadorLaboratorio") ||
                (observadorRoles.contains("Paciente") && consulta.getPacienteDNI().equals(consultaRol))){
                    res= ResponseEntity.ok().body(consulta);
            }
            else{
                res = new ResponseEntity<>(HttpStatus.UNAUTHORIZED); //401
            }

            return res;

        }
}
