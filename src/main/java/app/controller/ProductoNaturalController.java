package app.controller;

import app.dao.ProductoNaturalDAO;
import app.model.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
public class ProductoNaturalController {
    @Autowired
    ProductoNaturalDAO productoNaturalDAO;

    List<ProductoNaturalResponse> res;


    long totalPages;

    @GetMapping("/getInfoProductoNatural")
    public ResponseEntity<PaginationResponse<ProductoNaturalResponse>> getProductoNatural(
            @RequestParam String nombre,
            PageMetadata pageData
    ) throws SQLException {
        if(pageData.getPageSize()<1)
            return ResponseEntity.badRequest().build();

        ResponseEntity<PaginationResponse<ProductoNaturalResponse>> response = null;

        res=productoNaturalDAO.getProductoNatural(nombre,pageData);

        String query= "FROM (SELECT ProductoNatural.Nombre, ProductoNatural.idProductoNatural, ProductoNatural.Contraindicaciones, ProductoNatural.Posologia, ProductoNatural.Prospecto, ProductoNatural.Composicion, ANY_VALUE(URL.URL) AS URL FROM `ProductoNatural-URL` JOIN ProductoNatural JOIN URL ON URL.idURL = `ProductoNatural-URL`.idURL AND `ProductoNatural-URL`.idProductoNatural = ProductoNatural.idProductoNatural WHERE ProductoNatural.Nombre LIKE '%"+nombre+"%' GROUP BY ProductoNatural.Nombre) T";
        totalPages = productoNaturalDAO.getNumPages(query,pageData.getPageSize());
        pageData.setTotalPages(totalPages);

        // comprobamos que haya contenido para devolver sino error: 204
        if(totalPages==0)
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        //comprobamos que nos estén pidiendo un numPage correcto, sino error:400
        if(productoNaturalDAO.isNumPageInRange(pageData)){
            response = ResponseEntity.ok().body(new PaginationResponse<>(
                    res,
                    pageData));
            return response;
        }else{ //errror:400
            return response = ResponseEntity.badRequest().build();
        }



    }
}
