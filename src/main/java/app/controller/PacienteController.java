package app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import app.model.response.PaginationResponse;
import app.model.response.PacienteResponse;
import app.model.response.PageMetadata;
import app.dao.PacienteDAO;
import app.dao.TokenizacionDAO;
import app.service.UserService;
import app.service.PacienteService;


import java.util.List;

@RestController
public class PacienteController {
	@Autowired
	PacienteDAO pacienteDAO;
    @Autowired
    TokenizacionDAO tokDAO;
    @Autowired
    UserService userService;
    @Autowired
    PacienteService pacienteService;

    @GetMapping("/getPacientes/{dniDT}/{tokenUsuario}/{nombreFiltro}/{dniFiltro}/{tamanoPag}/{pagina}")
    public ResponseEntity<PaginationResponse<PacienteResponse>> getPacientes(
			@PathVariable String dniDT, @PathVariable Integer tokenUsuario, 
            @PathVariable String dniFiltro, @PathVariable String nombreFiltro,
            @PathVariable Long tamanoPag, @PathVariable Long pagina) throws Exception {

        ResponseEntity<PaginationResponse<PacienteResponse>> res = null;

        //No se puede llamar con ambos filtros a la vez
        if(!dniFiltro.equals("0") && !nombreFiltro.equals("0"))
                return ResponseEntity.badRequest().build();

        String observadorDNI = tokDAO.getDNI(tokenUsuario);

        //Observador no existe
        if(observadorDNI == null)
                return ResponseEntity.noContent().build();

        List<String> dniRoles = userService.getRoles(dniDT);

        //Solo doctores y trabajadores de laboratorio tienen pacientes
        if(!dniRoles.contains("Doctor") && !dniRoles.contains("TrabajadorLaboratorio"))
            return ResponseEntity.badRequest().build();
        
        List<String> observadorRoles = userService.getRoles(observadorDNI);
        
        //Solo doctores y trabajadores de laboratorio pueden consultar pacientes
        if(!observadorRoles.contains("Doctor") && !observadorRoles.contains("TrabajadorLaboratorio"))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        String rol, tabla;
        //Asumir que no se puede ser doctor y trabajador de laboratorio a la vez
        if(dniRoles.contains("Doctor")){
            rol = "DoctorDNI";
            tabla = "Consulta";
        }else{
            rol = "TrabajadorLaboratorioDNI";
            tabla = "Prueba";
        }
        
        Long numPages = pacienteService.getPagesGetPacientes(
            tabla, rol, dniFiltro, nombreFiltro, tamanoPag, dniDT);

        
        //dniDT no tiene pacientes
        if(numPages.equals(Long.valueOf(0)))
            return ResponseEntity.noContent().build();

        PageMetadata metadata = new PageMetadata(tamanoPag, pagina, numPages);

        if(pacienteDAO.isNumPageInRange(metadata)){  
            res = ResponseEntity.ok().body(new PaginationResponse<>(
                pacienteService.getPacientes(tabla, rol, dniFiltro, nombreFiltro, metadata, dniDT), 
                metadata)
            );
        }else{
            res = ResponseEntity.badRequest().build();
        }
        
        return res;
	}

    @GetMapping("/getAllPacientes/{tokenUsuario}/{nombreFiltro}/{dniFiltro}/{tamanoPag}/{pagina}")
    public ResponseEntity<PaginationResponse<PacienteResponse>> getAllPacientes(
			@PathVariable Integer tokenUsuario, 
            @PathVariable String nombreFiltro, @PathVariable String dniFiltro,
            @PathVariable Long tamanoPag, @PathVariable Long pagina) 
            throws Exception {

        ResponseEntity<PaginationResponse<PacienteResponse>> res = null;

        //No se puede llamar con ambos filtros a la vez
        if(!dniFiltro.equals("0") && !nombreFiltro.equals("0"))
                return ResponseEntity.badRequest().build();

        String observadorDNI = tokDAO.getDNI(tokenUsuario);

        //Observador no existe
        if(observadorDNI == null)
                return ResponseEntity.noContent().build();
        
        List<String> observadorRoles = userService.getRoles(observadorDNI);
        
        //Solo doctores pueden ver la lista completa de pacientes
        if(!observadorRoles.contains("Doctor") && (!observadorRoles.contains("TrabajadorLaboratorio")))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        
        Long numPages = pacienteService.getPagesGetAllPacientes(dniFiltro, nombreFiltro, tamanoPag);

        //No hay pacientes en la base de datos con ese filtro
        if(numPages.equals(Long.valueOf(0)))
            return ResponseEntity.noContent().build();

        PageMetadata metadata = new PageMetadata(tamanoPag, pagina, numPages);

        if(pacienteDAO.isNumPageInRange(metadata)){  
            res = ResponseEntity.ok().body(new PaginationResponse<>(
                pacienteService.getAllPacientes(dniFiltro, nombreFiltro, metadata), 
                metadata)
            );
        }else{
            res = ResponseEntity.badRequest().build();
        }
        
        return res;
	}
}