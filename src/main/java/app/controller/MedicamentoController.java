package app.controller;

import app.factory.PageMetadataFactory;
import app.factory.PaginationResponseFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import app.model.response.PaginationResponse;
import app.model.response.PageMetadata;
import app.model.response.MedicamentoResponse;
import app.dao.MedicamentoDAO;
import app.dao.TokenizacionDAO;
import app.service.MedicamentoService;
import app.service.UserService;

import java.util.List;

@RestController
public class MedicamentoController {
	@Autowired
    private MedicamentoDAO medicamentoDAO;
    @Autowired
    private TokenizacionDAO tokDAO;
    @Autowired
    private UserService userService;
    @Autowired
    private MedicamentoService medicamentoService;
    @Autowired
    private PageMetadataFactory pageMetadataFactory;
    @Autowired
    private PaginationResponseFactory paginationResponseFactory;


    @GetMapping("/getMedicamentos/{dniPaciente}/{tokenUsuario}/{tamanoPag}/{pagina}")
    public ResponseEntity<PaginationResponse<String>> getMedicamentos(
			@PathVariable String dniPaciente, @PathVariable Integer tokenUsuario, 
            @PathVariable Long tamanoPag, @PathVariable Long pagina) throws Exception {

        ResponseEntity<PaginationResponse<String>> res = null;

		Long numPages = medicamentoDAO.getNumPages("FROM Medicamento AS m JOIN `Paciente-Medicamento` AS pm ON m.idMedicamento = pm.idMedicamento WHERE pm.PacienteDNI=?"
                , tamanoPag, dniPaciente);
        String observadorDNI = tokDAO.getDNI(tokenUsuario);
            
        //Usuario no tiene medicamentos o el observador no existe
        if(numPages.equals(Long.valueOf(0)) || observadorDNI == null) {
            return ResponseEntity.noContent().build();
        }

        List<String> observadorRoles = userService.getRoles(observadorDNI);

        if(observadorRoles.contains("Doctor") || observadorRoles.contains("TrabajadorLaboratorio") ||
                observadorRoles.contains("Paciente") && dniPaciente.equals(observadorDNI)){

            PageMetadata metadata = pageMetadataFactory.newPageMetadata(tamanoPag,pagina,numPages);
            
            if(medicamentoDAO.isNumPageInRange(metadata)){
                res = ResponseEntity.ok().body(paginationResponseFactory.newPaginationResponse(
                    medicamentoService.getMedicamentos(metadata, dniPaciente), 
                    metadata)
                );
            }else{
                res = ResponseEntity.badRequest().build();
            }

        }else{
            res = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        return res;
	}

    @GetMapping("/getInfoMedicamento")
    public ResponseEntity<PaginationResponse<MedicamentoResponse>> getInfoMedicamento
        (@RequestParam String nombre, @RequestParam Long pageSize, @RequestParam Long pageNumber) throws Exception {
        
        ResponseEntity<PaginationResponse<MedicamentoResponse>> res = null;
        
        if(pageSize <= 0)
            return ResponseEntity.badRequest().build();

        Long numPages = medicamentoDAO.getNumPages("FROM (SELECT m.idMedicamento, Nombre, Contraindicaciones, Posologia, Prospecto, Composicion, ANY_VALUE(u.URL) AS URL " +
                "FROM Medicamento AS m LEFT JOIN `Medicamento-URL` AS mu ON m.idMedicamento = mu.idMedicamento " +
                "LEFT JOIN URL AS u ON u.idURL = mu.idURL WHERE m.Nombre LIKE CONCAT('%',?,'%') GROUP BY m.Nombre) q"
                , pageSize, nombre);
        
        //Si no existe el medicamento
        if(numPages.equals(Long.valueOf(0))) {
            return ResponseEntity.noContent().build();
        }

        PageMetadata metadata = pageMetadataFactory.newPageMetadata(pageSize,pageNumber,numPages);
            
            if(medicamentoDAO.isNumPageInRange(metadata)){
                res = ResponseEntity.ok().body(paginationResponseFactory.newPaginationResponse(
                    medicamentoService.getInfoMedicamento(metadata, nombre), 
                    metadata));
            }else{
                res = ResponseEntity.badRequest().build();
            }
        
        return res;
    }

}
