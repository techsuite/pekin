package app.controller;

import lombok.*;
import app.dao.TokenizacionDAO;
import app.dao.UserDAO;
import app.dao.UsuarioImpostorDAO;

import app.model.User;
import app.model.UsuarioImpostor;
import app.model.response.UserResponse;
import app.service.UserService;
import app.model.response.UsuarioImpostorResponse;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.sql.SQLException;
import java.util.List;

@RestController
public class UserController {
	@AllArgsConstructor
	@Getter
	private class Entero{private Integer token;}
	
	@Autowired
	UserDAO userDAO;
	@Autowired
	TokenizacionDAO tokDAO;
	@Autowired
	UserService userService;
	@Autowired
	UsuarioImpostorDAO usuarioImpostorDAO;

	// Assuming consistent database
	// i.e. same dni exists in both Usuario and Tokenizacion tables
    @GetMapping("/loginStatus/{dni}/{password}")
    public ResponseEntity<Entero> getLoginStatus(
			@PathVariable String dni, @PathVariable String password) throws Exception {

		User usuario = userDAO.getUser(dni);
		Entero token = new Entero(tokDAO.getToken(dni));

		return usuario != null && usuario.getContrasena().equals(password) 
			? ResponseEntity.ok().body(token) 
			: ResponseEntity.noContent().build();
	}

	@GetMapping("/loginStatus/impostor/{dni}/{token}")
	public ResponseEntity<UsuarioImpostorResponse> getDNIUsuarioASuplantar(
			@PathVariable String dni, @PathVariable Integer token) throws Exception{
		
		UsuarioImpostor usuarioImpostor = usuarioImpostorDAO.getUsuarioImpostor(dni, token);
		System.out.println(usuarioImpostor);
		return usuarioImpostor != null
			? ResponseEntity.ok().body(usuarioImpostorDAO.getTokenUsuarioSuplantado(usuarioImpostor))
			: ResponseEntity.noContent().build();
	}

	@GetMapping("/user/{token}")
	public ResponseEntity<UserResponse> getAtributos(@PathVariable Integer token) throws Exception{
		String dni = tokDAO.getDNI(token);

		if(dni == null)
			return ResponseEntity.noContent().build();

		User u = userDAO.getUser(dni);
		int edad= userService.getEdad(dni);
		return ResponseEntity.ok().body(
			new UserResponse(dni, u.getNombre(), u.getApellidos(), u.getGenero(),
				u.getCorreo_electronico(), edad)
		);
	}

	// Assuming every user has at least one role
	@GetMapping("/getRoles/{token}")
	public ResponseEntity<List<String>> getRoles(@PathVariable Integer token) throws SQLException{
		String dni = tokDAO.getDNI(token);

		if(dni == null)
			return ResponseEntity.noContent().build();

		List<String> roles = userService.getRoles(dni);

		return ResponseEntity.ok().body(roles);
	}

}

