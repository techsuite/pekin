package app.service;

import app.model.response.MedicamentoResponse;
import app.model.response.PageMetadata;
import app.dao.MedicamentoDAO;

import java.util.List;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class MedicamentoService {
    @Autowired
	MedicamentoDAO medicamentoDAO;

    public Long getPagesGetMedicamentos(Long tamanoPag, String dniPaciente) throws Exception {
        return medicamentoDAO.getNumPages(getCountQueryString(), tamanoPag, dniPaciente);
    }

    public List<String> getMedicamentos(PageMetadata metadata, String dniPaciente) throws SQLException{
        return medicamentoDAO.getPage(getSelectQueryString(), metadata, dniPaciente);
	}

    public List<MedicamentoResponse> getInfoMedicamento(PageMetadata metadata, String nombre) throws SQLException {
        return medicamentoDAO.getPage(MedicamentoResponse.class, 
            "SELECT m.idMedicamento, Nombre, Contraindicaciones, Posologia, Prospecto, Composicion, ANY_VALUE(u.URL) AS URL " +
            "FROM Medicamento AS m LEFT JOIN `Medicamento-URL` AS mu ON m.idMedicamento = mu.idMedicamento " +
            "LEFT JOIN URL AS u ON u.idURL = mu.idURL WHERE m.Nombre LIKE CONCAT('%',?,'%') GROUP BY m.Nombre",
            metadata, nombre);
    }



    private String getCountQueryString(){
        return "FROM Medicamento AS m JOIN `Paciente-Medicamento` AS pm ON m.idMedicamento=pm.idMedicamento WHERE pm.PacienteDNI=?";
    }

    private String getSelectQueryString(){
        return
            "SELECT m.Nombre FROM Medicamento AS m JOIN `Paciente-Medicamento` AS pm ON m.idMedicamento=pm.idMedicamento WHERE pm.PacienteDNI=?";
    }
}
