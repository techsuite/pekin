package app.service;

import app.model.response.PageMetadata;
import app.model.response.PacienteResponse;
import app.dao.PacienteDAO;

import java.util.List;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class PacienteService{
	@Autowired
	PacienteDAO pacienteDAO;

      public Long getPagesGetPacientes(
                  String tabla, String rol, 
                  String dniFiltro, String nombreFiltro,
                  Long tamanoPag, String dniDT) throws Exception{
            Long numPages = null;

            //No contar pacientes repetidos

            //Sin filtro
            if(dniFiltro.equals("0") && nombreFiltro.equals("0")){
                  numPages = pacienteDAO.getNumPages(
                        getCountQueryStringGetPacientes(tabla, rol, " "), tamanoPag, dniDT);
            //Filtro por DNI
            }else if(!dniFiltro.equals("0")){
                  numPages = pacienteDAO.getNumPages(
                        getCountQueryStringGetPacientes(tabla, rol, " WHERE dni=? "), 
                  tamanoPag, dniDT, dniFiltro);
            //Filtro por nombre
            }else{
                  numPages = pacienteDAO.getNumPages(
                        getCountQueryStringGetPacientes(tabla, rol, " WHERE nombre LIKE '%"+nombreFiltro+"%' "),
                  tamanoPag, dniDT);
            }

            return numPages;
      }

	public List<PacienteResponse> getPacientes(
                  String tabla, String rol, 
                  String dniFiltro, String nombreFiltro,
                  PageMetadata metadata, String dni) throws SQLException{
            
            List<PacienteResponse> l = null;

            //Sin filtro
            if(dniFiltro.equals("0") && nombreFiltro.equals("0")){
                  l = pacienteDAO.getPage(PacienteResponse.class, 
                              getSelectQueryStringGetPacientes(tabla, rol, " "), 
                        metadata, dni);
            //Filtro por DNI
            }else if(!dniFiltro.equals("0")){
                  l = pacienteDAO.getPage(PacienteResponse.class, 
                              getSelectQueryStringGetPacientes(tabla, rol, " WHERE dni=? "), 
                        metadata, dni, dniFiltro);
            //Filtro por nombre
            }else{
                  l = pacienteDAO.getPage(PacienteResponse.class, 
                              getSelectQueryStringGetPacientes(tabla, rol, " WHERE nombre LIKE '%"+nombreFiltro+"%'"),
                        metadata, dni);
            }
           
            return l;
	}

      public Long getPagesGetAllPacientes(String dniFiltro, String nombreFiltro, Long tamanoPag) throws Exception{
            Long numPages = null;

            //No contar pacientes repetidos

            //Sin filtro
            if(dniFiltro.equals("0") && nombreFiltro.equals("0")){
                  numPages = pacienteDAO.getNumPages(
                        getCountQueryStringGetAllPacientes(""), tamanoPag);
            //Filtro por DNI
            }else if(!dniFiltro.equals("0")){
                  numPages = pacienteDAO.getNumPages(
                        getCountQueryStringGetAllPacientes(" WHERE dni=?"), 
                        tamanoPag, dniFiltro);
            //Filtro por nombre
            }else{
                  numPages = pacienteDAO.getNumPages(
                        getCountQueryStringGetAllPacientes(" WHERE nombre LIKE '%"+nombreFiltro+"%'"),
                        tamanoPag);
            }

            return numPages;
      }

      public List<PacienteResponse> getAllPacientes(String dniFiltro, String nombreFiltro,
                  PageMetadata metadata) throws SQLException{

            List<PacienteResponse> l = null;

		//Sin filtro
            if(dniFiltro.equals("0") && nombreFiltro.equals("0")){
                  l = pacienteDAO.getPage(PacienteResponse.class, 
                              getSelectQueryStringGetAllPacientes(""), 
                        metadata);
            //Filtro por DNI
            }else if(!dniFiltro.equals("0")){
                  l = pacienteDAO.getPage(PacienteResponse.class, 
                              getSelectQueryStringGetAllPacientes(" WHERE dni=?"), 
                        metadata, dniFiltro);
            //Filtro por nombre
            }else{
                  l = pacienteDAO.getPage(PacienteResponse.class, 
                              getSelectQueryStringGetAllPacientes(" WHERE nombre LIKE '%"+nombreFiltro+"%'"),
                        metadata);
            }

            return l;
	}

      private String getSelectQueryStringGetPacientes(String tabla, String rol, String filtro){
            return
                "SELECT DNI as dni, genero, "+
                "TIMESTAMPDIFF(YEAR, fecha_nacimiento, NOW()) as edad, nombre "+
                "FROM Usuario JOIN "+
                "(SELECT * FROM "+tabla+" WHERE "+rol+"=?) T "+
                "ON PacienteDNI=DNI"+filtro+"GROUP BY dni";
      }

      private String getCountQueryStringGetPacientes(String tabla, String rol, String filtro){
            return
                "FROM (SELECT DNI as dni, genero, fecha_nacimiento, nombre FROM Usuario JOIN "+
                "(SELECT * FROM "+tabla+" WHERE "+rol+"=?) T "+
                "ON PacienteDNI=DNI"+filtro+"GROUP BY dni) T";
      }

      private String getSelectQueryStringGetAllPacientes(String filtro){
            return
                "SELECT DNI as dni, nombre, "+
                "TIMESTAMPDIFF(YEAR, fecha_nacimiento, NOW()) as edad, genero "+
                "FROM Paciente JOIN Usuario ON PacienteDNI=DNI"+filtro;
      }

      private String getCountQueryStringGetAllPacientes(String filtro){
            return "FROM (SELECT * FROM Paciente JOIN Usuario ON PacienteDNI=DNI"+filtro+") T";
      }


}
