package app.service;

import javax.mail.internet.MimeMessage;

import app.factory.SimpleFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service //Como si fuera un DAO; poner un @autowired
public class EmailService {

  //Inyectar autowired obj
  @Autowired
  private JavaMailSender envioCorreo;

  @Autowired
  private SimpleFactory simpleFactory;

  public void sendEmail(
    String correo_electronico,
    String mensaje,
    String asunto
  ) throws Exception {
    MimeMessage mimeMessage = envioCorreo.createMimeMessage();
    MimeMessageHelper helper = simpleFactory.mimeMessageHelper(mimeMessage);
    //He creao el objeto
    helper.setTo(correo_electronico);
    helper.setSubject(asunto);
    helper.setText(mensaje, true); //derecha true pq eso indica que es html, ermite ppner packs en el correo (ponerlo bonito)
    envioCorreo.send(mimeMessage);
  }
}
