package app.service;

import app.model.response.PageMetadata;
import app.model.response.ConsultaResponse;
import app.dao.ConsultaDAO;

import java.util.List;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ConsultaService{
	@Autowired
	ConsultaDAO consultaDAO;

    public Long getPagesGetConsultas(
                Long fechaFiltro, String doctorNombreFiltro,
                Long tamanoPag, String dniPaciente) throws Exception{
        
        Long numPages = null;

        //Sin filtro
        if(doctorNombreFiltro.equals("0") && fechaFiltro.equals(Long.valueOf(0))){
                numPages = consultaDAO.getNumPages(
                    getCountQueryString(""), tamanoPag, dniPaciente);
        //Filtro por DNI de doctor
        }else if(!doctorNombreFiltro.equals("0")){
                numPages = consultaDAO.getNumPages(
                    getCountQueryString(" AND Usuario.nombre LIKE CONCAT('%',?,'%')"), 
                tamanoPag, dniPaciente, doctorNombreFiltro);
        //Filtro por fecha (dia) de consulta
        }else{
                numPages = consultaDAO.getNumPages(
                    getCountQueryString(
                        " AND DATE(FechaRealizacion)=DATE(from_unixtime(?))"), 
                tamanoPag, dniPaciente, fechaFiltro/1000);
        }

        return numPages;
    }

    public List<ConsultaResponse> getConsultas(
            Long fechaFiltro, String doctorNombreFiltro,
            PageMetadata metadata, String dniPaciente) throws SQLException{
            
        List<ConsultaResponse> l = null;

        //Sin filtro
        if(doctorNombreFiltro.equals("0") && fechaFiltro.equals(Long.valueOf(0))){
                l = consultaDAO.getPage(ConsultaResponse.class, 
                            getSelectQueryString(""), 
                    metadata, dniPaciente);
        //Filtro por DNI de doctor
        }else if(!doctorNombreFiltro.equals("0")){
                l = consultaDAO.getPage(ConsultaResponse.class, 
                            getSelectQueryString(" AND Usuario.nombre LIKE CONCAT('%',?,'%')"), 
                    metadata, dniPaciente, doctorNombreFiltro);
        //Filtro por fecha (dia) de consulta
        }else{
                l = consultaDAO.getPage(ConsultaResponse.class, 
                    getSelectQueryString(
                        " AND DATE(FechaRealizacion)=DATE(from_unixtime(?))"), 
                    metadata, dniPaciente, fechaFiltro/1000);
        }
        
        return l;
	}

    private String getCountQueryString(String filtro){
        return "FROM Consulta AS c JOIN Usuario ON c.DoctorDNI=Usuario.DNI WHERE PacienteDNI=?"+filtro;
    }

    private String getSelectQueryString(String filtro){
        return
            "SELECT idConsulta, FechaRealizacion, DoctorDNI, Usuario.nombre as doctorNombre "+
            "FROM Consulta AS c JOIN Usuario ON c.DoctorDNI=Usuario.DNI WHERE PacienteDNI=?"+filtro;
    }
}
