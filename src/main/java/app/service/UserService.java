package app.service;

import app.model.*;
import app.dao.*;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class UserService{
	@Autowired
	UserDAO uDAO;
	@Autowired
	PacienteDAO pDAO;
	@Autowired
	DoctorDAO dDAO;
	@Autowired
	ImpostorDAO iDAO;
	@Autowired
	TrabajadorLaboratorioDAO tDAO;

	public List<String> getRoles(String dni) throws SQLException{
		List<String> roles = new ArrayList<>();

		Paciente p = pDAO.getPaciente(dni);
		if(p != null)
			roles.add("Paciente");

		Doctor d = dDAO.getDoctor(dni);
		if(d != null)
			roles.add("Doctor");

		Impostor i = iDAO.getImpostor(dni);
		if(i != null)
			roles.add("Impostor");

		TrabajadorLaboratorio t = tDAO.getTrabajadorLaboratorio(dni);
		if(t != null)
			roles.add("TrabajadorLaboratorio");

		return roles;
	}

	public int getEdad(String dni) throws SQLException{
		User u = uDAO.getUser(dni);
		Date fecha_nacimiento=u.getFecha_nacimiento();
		int edad = Period.between(fecha_nacimiento.toLocalDate(), LocalDate.now()).getYears();

		return edad;
	}

}
