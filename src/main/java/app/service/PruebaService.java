package app.service;

import app.model.PCR;
import app.model.response.PageMetadata;
import app.model.response.PruebaResponse;
import app.dao.PruebaDAO;
import app.dao.PCRDAO;

import java.util.List;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class PruebaService{
	@Autowired
	PruebaDAO pruebaDAO;
	@Autowired
	PCRDAO pcrDAO;

    public Long getPagesGetPruebas(
                Long fechaFiltro, String trabLabNombreFiltro, String tipoPruebaFiltro,
                Long tamanoPag, String dniPaciente) throws Exception{
        
        Long numPages = null;

        //Sin filtro
        if(tipoPruebaFiltro.equals("0") && trabLabNombreFiltro.equals("0") 
                && fechaFiltro.equals(Long.valueOf(0))){

            numPages = pruebaDAO.getNumPages(
                getCountQueryString(""), tamanoPag, dniPaciente
            );
        //Filtro por DNI de trabajador de laboratorio
        }else if(!trabLabNombreFiltro.equals("0")){
            numPages = pruebaDAO.getNumPages(
                getCountQueryString(" AND Usuario.nombre LIKE CONCAT('%',?,'%')"), 
                tamanoPag, dniPaciente, trabLabNombreFiltro
            );
        //Filtro por fecha (dia) de consulta
        }else if(!fechaFiltro.equals(Long.valueOf(0))){
            numPages = pruebaDAO.getNumPages(
                getCountQueryString(
                    " AND DATE(FechaRealizacion)=DATE(from_unixtime(?))"), 
                tamanoPag, dniPaciente, fechaFiltro/1000
            );
        //Filtro por tipo de prueba
        }else{
            numPages = pruebaDAO.getNumPages(
                getCountQueryStringTipoPrueba(tipoPruebaFiltro), tamanoPag, dniPaciente
            );
        }

        return numPages;
    }

    public List<PruebaResponse> getPruebas(
            Long fechaFiltro, String trabLabDNIFiltro, String tipoPruebaFiltro,
            PageMetadata metadata, String dniPaciente) throws SQLException{
            
        List<PruebaResponse> l = null;

        if(tipoPruebaFiltro.equals("0")){
            //Sin filtro
            if(trabLabDNIFiltro.equals("0") && fechaFiltro.equals(Long.valueOf(0))){
                l = pruebaDAO.getPage(
                    PruebaResponse.class, getSelectQueryString(""), metadata, dniPaciente);
            //Filtro por DNI de trabajador de laboratorio
            }else if(!trabLabDNIFiltro.equals("0")){
                l = pruebaDAO.getPage(
                    PruebaResponse.class, getSelectQueryString(" AND Usuario.nombre LIKE CONCAT('%',?,'%')"), 
                    metadata, dniPaciente, trabLabDNIFiltro
                );
            //Filtro por fecha (dia) de consulta
            }else{
                l = pruebaDAO.getPage(
                    PruebaResponse.class, getSelectQueryString(
                        " AND DATE(FechaRealizacion)=DATE(from_unixtime(?))"), 
                    metadata, dniPaciente, fechaFiltro/1000
                );
            }

            for(PruebaResponse p : l){
                PCR pcr = pcrDAO.getPCR(p.getIdPrueba());
                // De momento no hay más pruebas aparte de AnalisisSangre
                // suponer que si no es pcr es análisis de sangre
                p.setTipoPrueba(pcr != null ? "PCR" : "AnalisisSangre");
            }

        }else{
            l = pruebaDAO.getPage(
                PruebaResponse.class, getSelectQueryStringTipoPrueba(tipoPruebaFiltro), 
                metadata, dniPaciente
            );
        }
        
        
        return l;
	}

    private String getCountQueryString(String filtro){
        return "FROM Prueba JOIN Usuario ON Prueba.TrabajadorLaboratorioDNI=Usuario.DNI WHERE PacienteDNI=?"+filtro;
    }

    private String getCountQueryStringTipoPrueba(String tipoPrueba){
        return "FROM Prueba JOIN "+tipoPrueba+" ON Prueba.idPrueba="+tipoPrueba+".idPrueba "+
            "WHERE PacienteDNI=?";
    }

    private String getSelectQueryString(String filtro){
        return
            "SELECT idPrueba, "+
            "FechaRealizacion as fechaRealizacion, nombre as trabLabNombre "+
            "FROM Prueba JOIN Usuario ON Prueba.TrabajadorLaboratorioDNI=Usuario.DNI "+
            "WHERE PacienteDNI=?"+filtro;
    }

    private String getSelectQueryStringTipoPrueba(String tipoPrueba){
        return
            "SELECT Prueba.idPrueba, \""+tipoPrueba+"\" as tipoPrueba, "+
             "FechaRealizacion as fechaRealizacion, "+
            "nombre as trabLabNombre "+
            "FROM Prueba JOIN Usuario JOIN "+tipoPrueba+
            " ON Prueba.idPrueba="+tipoPrueba+".idPrueba "+
            "AND Prueba.TrabajadorLaboratorioDNI=Usuario.DNI"+
            " WHERE PacienteDNI=?";
    }
}
