package app.service;

import app.model.response.EnfermedadResponse;
import app.model.response.PageMetadata;
import app.dao.EnfermedadDAO;

import java.util.List;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class EnfermedadService {
    @Autowired
	EnfermedadDAO enfermedadDAO;

    public Long getPagesGetEnfermedades(Long tamanoPag, String dniPaciente) throws Exception{
        return enfermedadDAO.getNumPages(getCountQueryString(), tamanoPag, dniPaciente);
    }

    public List<String> getEnfermedades(PageMetadata metadata, String dniPaciente) throws SQLException{
        return enfermedadDAO.getPage(getSelectQueryString(), metadata, dniPaciente);
	}

    private String getCountQueryString(){
        return "FROM Enfermedad AS e JOIN `Paciente-Enfermedad` AS pe ON e.idEnfermedad=pe.idEnfermedad WHERE pe.PacienteDNI=?";
    }

    private String getSelectQueryString(){
        return
            "SELECT e.Nombre FROM Enfermedad AS e JOIN `Paciente-Enfermedad` AS pe ON e.idEnfermedad=pe.idEnfermedad WHERE pe.PacienteDNI=?";
    }

    public List<EnfermedadResponse> getInfoEnfermedad(PageMetadata metadata, String nombre) throws SQLException {
        return enfermedadDAO.getPage(EnfermedadResponse.class, 
            "SELECT em.idEnfermedad, Nombre, Sintomas, Recomendaciones, ANY_VALUE(u.URL) AS URL " +
            "FROM Enfermedad AS em LEFT JOIN `Enfermedad-URL` AS eu ON em.idEnfermedad = eu.idEnfermedad " +
            "LEFT JOIN URL AS u ON u.idURL = eu.idURL WHERE em.Nombre LIKE CONCAT('%',?,'%') GROUP BY em.Nombre",
            metadata, nombre);
    }


}
