package app.factory;

import javax.mail.internet.MimeMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class SimpleFactory {

  public MimeMessageHelper mimeMessageHelper(MimeMessage mimeMessage) {
    return new MimeMessageHelper(mimeMessage, "utf-8");
  }
}
