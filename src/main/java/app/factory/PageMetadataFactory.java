package app.factory;

import org.springframework.stereotype.Component;

import app.model.response.PageMetadata;

@Component
public class PageMetadataFactory {
    public PageMetadata newPageMetadata (Long pageSize, Long pageNumber,Long totalPages){
        return new PageMetadata(pageSize, pageNumber, totalPages);
    }
}
