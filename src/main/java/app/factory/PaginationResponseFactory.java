package app.factory;

import java.util.List;

import org.springframework.stereotype.Component;

import app.model.response.PageMetadata;
import app.model.response.PaginationResponse;

@Component
public class PaginationResponseFactory <T>{
    public PaginationResponse<T> newPaginationResponse (List<T> data, PageMetadata metadata){
        return new PaginationResponse<> (data, metadata);
    }
}
