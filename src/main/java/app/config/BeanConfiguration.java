package app.config;

import java.util.Set;
import org.apache.commons.dbutils.QueryRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.AbstractRequestLoggingFilter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class BeanConfiguration {

  @Bean
  public QueryRunner queryRunner() {
    return new QueryRunner();
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .host("pekin.juliocastrodev.duckdns.org")
      .protocols(Set.of("https"))
      .select()
      .apis(RequestHandlerSelectors.basePackage("app.controller"))
      .paths(PathSelectors.any())
      .build();
  }

  @Bean
  public AbstractRequestLoggingFilter logFilter() {
    return new CustomLogsFilter();
  }
}
