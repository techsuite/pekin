SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS Tokenizacion;
CREATE TABLE Tokenizacion(
DNI varchar(20),
Token int UNIQUE AUTO_INCREMENT NOT NULL,
PRIMARY KEY (DNI, Token),
FOREIGN KEY (DNI) REFERENCES Usuario(DNI)
);

DROP TABLE IF EXISTS Usuario;
CREATE TABLE Usuario(
DNI varchar(20),
nombre varchar(60),
contrasena varchar(60),
apellidos varchar(60),
edad int,
genero varchar(10),
correo_electronico varchar(60),
PRIMARY KEY (DNI)
);



DROP TABLE IF EXISTS Paciente;
CREATE TABLE Paciente(
DNI varchar(20),
PRIMARY KEY (DNI),
FOREIGN KEY (DNI) REFERENCES Usuario(DNI)
);

DROP TABLE IF EXISTS Doctor;
CREATE TABLE Doctor(
DNI varchar(20),
PRIMARY KEY (DNI),
FOREIGN KEY (DNI) REFERENCES Usuario(DNI)
);

DROP TABLE IF EXISTS Impostor;
CREATE TABLE Impostor(
DNI varchar(20),
PRIMARY KEY (DNI),
FOREIGN KEY (DNI) REFERENCES Usuario(DNI)
);

DROP TABLE IF EXISTS TrabajadorLaboratorio;
CREATE TABLE TrabajadorLaboratorio(
DNI varchar(20),
PRIMARY KEY (DNI),
FOREIGN KEY (DNI) REFERENCES Usuario(DNI)
);

SET FOREIGN_KEY_CHECKS=1;

INSERT INTO Usuario VALUES 
("02565323J", "Javier", "jg2000", "de Andrés González", 20, "Varon", "javier.dandresg@alumnos.upm.es"),
("07939829V", "Miguel", "mg2000", "Ceballos Gomez", 20, "Varon", "m.ceballos@alumnos.upm.es");

INSERT INTO Tokenizacion VALUES
("02565323J", 1),
("07939829V", 2);

INSERT INTO Doctor VALUES
("02565323J"),
("07939829V");

INSERT INTO Paciente VALUES
("07939829V");

INSERT INTO TrabajadorLaboratorio VALUES
("02565323J");

INSERT INTO Impostor VALUES
("07939829V");
