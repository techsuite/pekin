package app.dao;

import app.model.Prueba;
import app.model.response.AnalisisSangreDetallesResponse;
import app.model.response.PCRDetallesResponse;

import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PruebaDAOTest extends GenericDaoTest {
  @Autowired
  private PruebaDAO pruebaDAO;
  
  @Test
  public void getPrueba_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    pruebaDAO.getPrueba(1);
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq(1));
  }

  @Test(expected = Exception.class)
  public void getPrueba_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), any(Integer.class))).thenThrow(Exception.class);
    pruebaDAO.getPrueba(1);
  }

  @Test
  public void getPrueba_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    Prueba p = pruebaDAO.getPrueba(1);
    verify(queryRunner, never()).query(any(), anyString(), any(), any(Integer.class));
    Assert.assertNull(p);
  }

  @Test
  public void getPCRDetalles_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    pruebaDAO.getPCRDetalles(1);
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq(1));
  }

  @Test(expected = Exception.class)
  public void getPCRDetalles_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), any(Integer.class))).thenThrow(Exception.class);
    pruebaDAO.getPCRDetalles(1);
  }

  @Test
  public void getPCRDetalles_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    PCRDetallesResponse pcrResp = pruebaDAO.getPCRDetalles(1);
    verify(queryRunner, never()).query(any(), anyString(), any(), any(Integer.class));
    Assert.assertNull(pcrResp);
  }

  @Test
  public void getAnalisisSangreDetalles_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    pruebaDAO.getAnalisisSangreDetalles(1);
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq(1));
  }

  @Test(expected = Exception.class)
  public void getAnalisisSangreDetalles_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), any(Integer.class))).thenThrow(Exception.class);
    pruebaDAO.getAnalisisSangreDetalles(1);
  }

  @Test
  public void getAnalisisSangreDetalles_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    AnalisisSangreDetallesResponse anSangResp = pruebaDAO.getAnalisisSangreDetalles(1);
    verify(queryRunner, never()).query(any(), anyString(), any(), any(Integer.class));
    Assert.assertNull(anSangResp);
  }
}
