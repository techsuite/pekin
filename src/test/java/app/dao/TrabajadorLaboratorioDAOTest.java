package app.dao;

import app.model.TrabajadorLaboratorio;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TrabajadorLaboratorioDAOTest extends GenericDaoTest {
  @Autowired
  private TrabajadorLaboratorioDAO trabajadorLaboratorioDAO;
  
  @Test
  public void getTrabajadorLaboratorio_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    trabajadorLaboratorioDAO.getTrabajadorLaboratorio("dummyDNI");
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq("dummyDNI"));
  }

  @Test(expected = Exception.class)
  public void getTrabajadorLaboratorio_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString())).thenThrow(Exception.class);
    trabajadorLaboratorioDAO.getTrabajadorLaboratorio("dummyDNI");
  }

  @Test
  public void getTrabajadorLaboratorio_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    TrabajadorLaboratorio t = trabajadorLaboratorioDAO.getTrabajadorLaboratorio("dummyDNI");
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(t);
  }
}
