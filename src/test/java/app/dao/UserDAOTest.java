package app.dao;

import app.model.User;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDAOTest extends GenericDaoTest {
  @Autowired
  private UserDAO userDAO;
  
  @Test
  public void getUserByDNI_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    userDAO.getUser("dummyDNI");
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq("dummyDNI"));
  }

  @Test(expected = Exception.class)
  public void getUserByDNI_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString())).thenThrow(Exception.class);
    userDAO.getUser("dummyDNI");
  }

  @Test
  public void getUserByDNI_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    User u = userDAO.getUser("dummyDNI");
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(u);
  }

  @Test
  public void setPassword_whenCalled_shouldExecuteRunnerUpdate() throws Exception {
    userDAO.setPassword("dummyDNI", "dummyPassword");
    verify(queryRunner).update(eq(connection),anyString(),eq("dummyPassword"),eq("dummyDNI"));
  }

  @Test(expected = Exception.class)
  public void setPassword_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.update(eq(connection), anyString(), anyString(), anyString())).thenThrow(Exception.class);
    userDAO.setPassword("dummyDNI", "dummyPassword");
  }

  @Test
  public void setPassword_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    userDAO.setPassword("dummyDNI", "dummyPassword");
    verify(queryRunner, never()).update(eq(connection), anyString(), anyString(), anyString());
  }
}
