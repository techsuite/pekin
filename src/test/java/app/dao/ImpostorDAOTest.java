package app.dao;

import app.model.Impostor;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImpostorDAOTest extends GenericDaoTest {
  @Autowired
  private ImpostorDAO impostorDAO;
  
  @Test
  public void getImpostor_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    impostorDAO.getImpostor("dummyDNI");
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq("dummyDNI"));
  }

  @Test(expected = Exception.class)
  public void getImpostor_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString())).thenThrow(Exception.class);
    impostorDAO.getImpostor("dummyDNI");
  }

  @Test
  public void getImpostor_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    Impostor i = impostorDAO.getImpostor("dummyDNI");
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(i);
  }
}
