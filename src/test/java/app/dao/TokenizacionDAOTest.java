package app.dao;

import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;
import app.model.Tokenizacion;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenizacionDAOTest extends GenericDaoTest {
  @Autowired
  private TokenizacionDAO tokDAO;
  
  @Test
  public void getToken_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    tokDAO.getToken("dummyDNI");
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq("dummyDNI"));
  }

  @Test(expected = Exception.class)
  public void getToken_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString())).thenThrow(Exception.class);
    tokDAO.getToken("dummyDNI");
  }

  @Test
  public void getToken_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    Integer token = tokDAO.getToken("dummyDNI");
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(token);
  }

  @Test
  public void getToken_whenCalledWithExistingDNI_shouldReturnToken() throws Exception{
    when(queryRunner.query(any(), anyString(), any(BeanHandler.class), anyString()))
    .thenReturn(new Tokenizacion("dummyDNI", 1));
    Integer token = tokDAO.getToken("dummyDNI");
    Assert.assertEquals(Integer.valueOf(1), token);
  }

  @Test
  public void getDNI_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    tokDAO.getDNI(0);
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq(0));
  }

  @Test(expected = Exception.class)
  public void getDNI_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString())).thenThrow(Exception.class);
    tokDAO.getDNI(0);
  }

  @Test
  public void getDNI_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    String dni = tokDAO.getDNI(0);
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(dni);
  }

  @Test
  public void getDNI_whenCalledWithExistingToken_shouldReturnDNI() throws Exception{
    when(queryRunner.query(any(), anyString(), any(BeanHandler.class), any(Integer.class)))
    .thenReturn(new Tokenizacion("dummyDNI", 1));

    String dni = tokDAO.getDNI(1);
    Assert.assertEquals("dummyDNI", dni);
  }

}
