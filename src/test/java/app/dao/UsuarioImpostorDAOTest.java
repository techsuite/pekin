package app.dao;

import app.model.UsuarioImpostor;
import app.model.response.UsuarioImpostorResponse;

import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioImpostorDAOTest extends GenericDaoTest {
  @Autowired
  private UsuarioImpostorDAO usuarioImpostorDAO;
  
  @Test
  public void getUsuarioImpostor_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    usuarioImpostorDAO.getUsuarioImpostor("dummyDNI", 0);
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq("dummyDNI"), eq(0));
  }

  @Test(expected = Exception.class)
  public void getUsuarioImpostor_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString(), any(Integer.class)))
    .thenThrow(Exception.class);

    usuarioImpostorDAO.getUsuarioImpostor("dummyDNI", 0);
  }

  @Test
  public void getUsuarioImpostor_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    UsuarioImpostor usuarioImpostor = usuarioImpostorDAO.getUsuarioImpostor("dummyDNI", 0);
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString(), any(Integer.class));
    Assert.assertNull(usuarioImpostor);
  }

  @Test
  public void getTokenUsuarioSuplantado_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    UsuarioImpostor u = new UsuarioImpostor();
    usuarioImpostorDAO.getTokenUsuarioSuplantado(u);
    verify(queryRunner).query(
      eq(connection),anyString(),any(BeanHandler.class),eq(u.getToken()),eq(u.getImpostor_ImpostorDNI()));
  }

  @Test(expected = Exception.class)
  public void getTokenUsuarioSuplantado_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), any(Integer.class), anyString()))
    .thenThrow(Exception.class);

    usuarioImpostorDAO.getTokenUsuarioSuplantado(new UsuarioImpostor());
  }

  @Test
  public void getTokenUsuarioSuplantado_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    UsuarioImpostorResponse ur = usuarioImpostorDAO.getTokenUsuarioSuplantado(new UsuarioImpostor());
    verify(queryRunner, never()).query(any(), anyString(), any(), any(Integer.class), anyString());
    Assert.assertNull(ur);
  }

}
