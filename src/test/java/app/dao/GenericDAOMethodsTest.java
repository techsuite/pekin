package app.dao;

import java.sql.SQLException;
import java.sql.Connection;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import app.model.response.PageMetadata;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GenericDAOMethodsTest extends GenericDaoTest {
  @Autowired
  private GenericDAO genericDAO;
  @Mock
  private PageMetadata metadata;

  @Test
  public void getNumPages_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    genericDAO.getNumPages("dummyQuery", (long)0);
    verify(queryRunner).query(eq(connection),anyString(),any(ScalarHandler.class),any(Object.class));
  }

  @Test(expected = Exception.class)
  public void getNumPages_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(
      any(Connection.class), anyString(), any(), any(Object.class))).thenThrow(Exception.class);
    genericDAO.getNumPages("dummyQuery", (long)0);
  }

  @Test
  public void getNumPages_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    Long numPages = genericDAO.getNumPages("dummyQuery", (long)0);
    verify(queryRunner, never()).query(any(Connection.class), anyString(), any(), any(Object.class));
    Assert.assertNull(numPages);
  }

  @Test
  public void getNumPages_whenCalledAndCorrectQuery_shouldReturnNumPages() throws Exception{
    when(queryRunner.query(eq(connection), anyString(), any(ScalarHandler.class), any(Object.class)))
    .thenReturn(Long.valueOf(0));
    Long numPages = genericDAO.getNumPages("dummyQuery", (long)0);
    Assert.assertNotNull(numPages);
  }
  
  @Test
  public void isNumPageInRange_whenCalled_shouldCallGetters(){
    when(metadata.getPageNumber()).thenReturn((long)1);
    when(metadata.getTotalPages()).thenReturn((long)1);
    boolean isInRange = genericDAO.isNumPageInRange(metadata);
    verify(metadata, times(2)).getPageNumber();
    verify(metadata, times(1)).getTotalPages();
    Assert.assertTrue(isInRange);
  }

  @Test
  public void isNumPageInRange_whenPageLessThanOne_shouldReturnFalse(){
    when(metadata.getPageNumber()).thenReturn((long)0);
    boolean isInRange = genericDAO.isNumPageInRange(metadata);
    verify(metadata).getPageNumber();
    Assert.assertFalse(isInRange);
  }

  @Test
  public void isNumPageInRange_whenPageMoreThanTotal_shouldReturnFalse(){
    when(metadata.getPageNumber()).thenReturn((long)2);
    when(metadata.getTotalPages()).thenReturn((long)1);
    boolean isInRange = genericDAO.isNumPageInRange(metadata);
    verify(metadata, times(2)).getPageNumber();
    verify(metadata, times(1)).getTotalPages();
    Assert.assertFalse(isInRange);
  }

  @Test
  public void getPages_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    genericDAO.getPage(Object.class, "dummyQuery", metadata);
    verify(queryRunner).query(
      eq(connection),anyString(),any(BeanListHandler.class), any(Object.class));
  }

  @Test(expected = Exception.class)
  public void getPages_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(
      any(Connection.class), anyString(), 
      any(BeanListHandler.class), any(Object.class)))
      .thenThrow(Exception.class);
    genericDAO.getPage(Object.class, "dummyQuery", metadata);
  }

  @Test
  public void getPages_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    List<Object> l = genericDAO.getPage(Object.class, "dummyQuery", metadata);

    verify(queryRunner, never()).query(
      any(Connection.class), anyString(), any(BeanListHandler.class), any(Object.class));
      
    Assert.assertNull(l);
  }

}


