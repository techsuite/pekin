package app.dao;

import app.model.Doctor;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DoctorDAOTest extends GenericDaoTest {
  @Autowired
  private DoctorDAO doctorDAO;
  
  @Test
  public void getDoctor_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    doctorDAO.getDoctor("dummyDNI");
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq("dummyDNI"));
  }

  @Test(expected = Exception.class)
  public void getDoctor_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString())).thenThrow(Exception.class);
    doctorDAO.getDoctor("dummyDNI");
  }

  @Test
  public void getDoctor_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    Doctor d = doctorDAO.getDoctor("dummyDNI");
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(d);
  }
}
