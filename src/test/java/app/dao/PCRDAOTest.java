package app.dao;

import app.model.PCR;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PCRDAOTest extends GenericDaoTest {
  @Autowired
  private PCRDAO pcrDAO;
  
  @Test
  public void getPCR_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    pcrDAO.getPCR(0);
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq(0));
  }

  @Test(expected = Exception.class)
  public void getPCR_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), any(Integer.class))).thenThrow(Exception.class);
    pcrDAO.getPCR(0);
  }

  @Test
  public void getDoctor_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    PCR pcr = pcrDAO.getPCR(0);
    verify(queryRunner, never()).query(any(), anyString(), any(), any(Integer.class));
    Assert.assertNull(pcr);
  }
}
