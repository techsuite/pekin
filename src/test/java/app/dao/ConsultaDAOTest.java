package app.dao;

import app.dao.ConsultaDAO;
import app.model.Consulta;

import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest

public class ConsultaDAOTest extends GenericDaoTest {

  @Autowired
  private ConsultaDAO consultaDAO;

  //Todo OK
  @Test
  public void getConsulta_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    consultaDAO.getConsulta(123);
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq(123));
  }
  
  @Test(expected = Exception.class)
  public void getConsulta_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString())).thenThrow(Exception.class);
    consultaDAO.getConsulta(123);
  }
  
  @Test
  public void getConsulta_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    Consulta i = consultaDAO.getConsulta(123);
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(i);
  }

}
