package app.dao;

import static org.mockito.Mockito.*;

import app.dao.connector.Connector;
import app.model.Otp;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.Assert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@WebMvcTest(OtpDAO.class)
public class OtpDAOTest {

  @Autowired
  OtpDAO otpDAO;

  @MockBean
  protected Connector connector;

  @MockBean
  protected QueryRunner queryRunner;

  @Mock
  protected Connection connection;

  @Mock
  protected Statement statement;

  @Mock
  protected PreparedStatement preparedStatement;

  @Mock
  protected ResultSet resultSet;

  @Test
  public void getOtp_whenCalling_withDNI_shouldExecuteRunnerQuery()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    otpDAO.getOtp("dummyDNI");
    verify(queryRunner)
      .query(any(), anyString(), any(BeanHandler.class), eq("dummyDNI"));
  }

  @Test(expected = Exception.class)
  public void getOtp_whenCalledWithDNIAndAnExceptionIsThrown_shouldThrowIt()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    when(queryRunner.query(any(), anyString(), any(), anyString()))
      .thenThrow(Exception.class);
    otpDAO.getOtp("dummyDNI");
  }

  @Test
  public void getOtp_whenCalledWithDNIAndCantConnect_shouldReturnNull()
    throws Exception {
    when(connector.getConnection()).thenThrow(SQLException.class);
    Otp otp = otpDAO.getOtp("dummyDNI");
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(otp);
  }

  @Test
  public void getOtp_whenCallingWithId_shouldExecuteRunnerQuery()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    otpDAO.getOtp(3);
    verify(queryRunner)
      .query(any(), anyString(), any(BeanHandler.class), eq(3));
  }

  @Test(expected = Exception.class)
  public void getOtp_whenCalledWithIdAndAnExceptionIsThrown_shouldThrowIt()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    when(queryRunner.query(any(), anyString(), any(), anyString()))
      .thenThrow(Exception.class);
    otpDAO.getOtp(3);
  }

  @Test
  public void getOtp_whenCalledWithIdAndCantConnect_shouldReturnNull()
    throws Exception {
    when(connector.getConnection()).thenThrow(SQLException.class);
    Otp otp = otpDAO.getOtp(3);
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(otp);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getOtp_whenCalledWithNoStringNoInteger_shouldThrowIllegalArgumentException()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    otpDAO.getOtp(new Object());
  }

  @Test
  public void updateOtp_whenCalled_shouldExecuteRunnerUpdate()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    otpDAO.updateOtp(
      (Integer) 1,
      12345,
      new Timestamp(System.currentTimeMillis() + 40000),
      "tipo1"
    );
    verify(queryRunner)
      .update(
        eq(connection),
        anyString(),
        eq(12345),
        any(),
        eq("tipo1"),
        eq(1)
      );
  }

  @Test(expected = Exception.class)
  public void updateOtp_whenCalledAndAnExceptionIsThrown_shouldThrowIt()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    when(queryRunner.query(any(), anyString(), any(), anyString()))
      .thenThrow(Exception.class);
    otpDAO.updateOtp(
      1,
      54321,
      new Timestamp(System.currentTimeMillis() + 40000),
      "tipo1"
    );
  }

  @Test
  public void removeOtp_whenCalled_shouldExecuteRunnerUpdate()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    Otp otp = new Otp(
      1,
      "dummyDNI",
      12345,
      new Timestamp(System.currentTimeMillis() + 40000),
      "tipo1"
    );
    otpDAO.removeOtp(otp);
    verify(queryRunner).update(eq(connection), anyString(), eq(otp.getIdOTP()));
  }

  @Test(expected = Exception.class)
  public void removeOtp_whenCalledAndAnExceptionIsThrown_shouldThrowIt()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);
    when(queryRunner.query(any(), anyString(), any(), anyString()))
      .thenThrow(Exception.class);
    Otp otp = new Otp(
      1,
      "dummyDNI",
      12345,
      new Timestamp(System.currentTimeMillis() + 40000),
      "tipo1"
    );
    otpDAO.removeOtp(otp);
  }

  @Test
  public void insertOtp_whenCalled_shouldCallRunnerWithCorrectParams()
    throws Exception {
    when(connector.getConnection()).thenReturn(connection);

    Timestamp dummyTimestamp = new Timestamp(System.currentTimeMillis());
    otpDAO.insertOtp("dummyDni", 123, dummyTimestamp, "Email");

    verify(queryRunner)
      .insert(
        eq(connection),
        anyString(),
        any(ScalarHandler.class),
        eq(123),
        eq(dummyTimestamp),
        eq("Email"),
        eq("dummyDni")
      );
  }
}
