package app.dao;

import app.dao.connector.Connector;
import app.model.response.PageMetadata;
import app.model.response.ProductoNaturalResponse;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest

public class ProductoNaturalDAOTest extends GenericDaoTest {


    @Autowired
    private ProductoNaturalDAO productoNaturalDAO;



  /*  //Todo OK
    @Test
    public void getProductoNatural_whenCalled_shouldExecuteGetPage() throws Exception {
        PageMetadata aux= new PageMetadata(1l,1l,1l);
        productoNaturalDAO.getProductoNatural("cicuta", aux);
        verify(productoNaturalDAO).getPage(any(),anyString(),eq(aux));
    }*/

    @Test(expected = Exception.class)
    public void getProductoNatural_whenCalledAnExceptionIsThrown_shouldThrowIt() throws Exception {
        when(queryRunner.query(any(), anyString(), any(), anyString())).thenThrow(Exception.class);
        productoNaturalDAO.getProductoNatural("cicuta",new PageMetadata(1l,1l,1l));
    }

    @Test
    public void getProductoNatural_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
        when(connector.getConnection()).thenThrow(SQLException.class);
        List<ProductoNaturalResponse> i = productoNaturalDAO.getProductoNatural("cicuta",new PageMetadata(1l,1l,1l));
        verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
        Assert.assertNull(i);
    }
}
