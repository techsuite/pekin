package app.dao;

import app.model.Paciente;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PacienteDAOTest extends GenericDaoTest {
  @Autowired
  private PacienteDAO pacienteDAO;
  
  @Test
  public void getPaciente_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    pacienteDAO.getPaciente("dummyDNI");
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq("dummyDNI"));
  }

  @Test(expected = Exception.class)
  public void getPaciente_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), anyString())).thenThrow(Exception.class);
    pacienteDAO.getPaciente("dummyDNI");
  }

  @Test
  public void getPaciente_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    Paciente p = pacienteDAO.getPaciente("dummyDNI");
    verify(queryRunner, never()).query(any(), anyString(), any(), anyString());
    Assert.assertNull(p);
  }
}
