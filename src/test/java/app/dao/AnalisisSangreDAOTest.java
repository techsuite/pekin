package app.dao;

import app.model.AnalisisSangre;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AnalisisSangreDAOTest extends GenericDaoTest {
  @Autowired
  private AnalisisSangreDAO anSangDAO;
  
  @Test
  public void getAnalisisSangre_whenCalled_shouldExecuteRunnerQuery() throws Exception {
    anSangDAO.getAnalisisSangre(0);
    verify(queryRunner).query(eq(connection),anyString(),any(BeanHandler.class),eq(0));
  }

  @Test(expected = Exception.class)
  public void getAnalisisSangre_whenCalledAndAnExceptionIsThrown_shouldThrowIt() throws Exception {
    when(queryRunner.query(any(), anyString(), any(), any(Integer.class))).thenThrow(Exception.class);
    anSangDAO.getAnalisisSangre(0);
  }

  @Test
  public void getAnalisisSangre_whenCalledAndCantConnect_shouldReturnNull() throws Exception{
    when(connector.getConnection()).thenThrow(SQLException.class);
    AnalisisSangre anSang = anSangDAO.getAnalisisSangre(0);
    verify(queryRunner, never()).query(any(), anyString(), any(), any(Integer.class));
    Assert.assertNull(anSang);
  }
}
