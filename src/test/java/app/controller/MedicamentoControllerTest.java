package app.controller;

import app.dao.MedicamentoDAO;
import app.dao.TokenizacionDAO;
import app.factory.PageMetadataFactory;
import app.factory.PaginationResponseFactory;
import app.model.Medicamento;
import app.model.response.MedicamentoResponse;
import app.model.response.PageMetadata;
import app.model.response.PaginationResponse;
import app.service.MedicamentoService;
import app.service.UserService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MedicamentoController.class)
public class MedicamentoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private MedicamentoDAO medicamentoDAO;
    @MockBean
    private MedicamentoService medicamentoService;
    @MockBean
    private TokenizacionDAO tokenizacionDAO;
    @MockBean
    private UserService userService;
    @MockBean
    private PageMetadataFactory pageMetadataFactory;
    @MockBean
    private PaginationResponseFactory paginationResponseFactory;

    @Test
    public void getMedicamentos_whenCallingWithDoctorAndPageInRange_shouldReturnPage() throws Exception {
        List<String> listaMed=List.of("Medicamento1", "Medicamento2");
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        PaginationResponse paginationResponse =new PaginationResponse(listaMed,pageMetadata);
        int tokenUsuario=1;
        when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(tokenizacionDAO.getDNI(tokenUsuario)).thenReturn("dummyDocotorDNI");
        when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
        when(pageMetadataFactory.newPageMetadata(anyLong(),anyLong(),anyLong())).thenReturn(pageMetadata);
        when(medicamentoDAO.isNumPageInRange(any())).thenReturn(true);
        when(medicamentoService.getMedicamentos(any(),anyString())).thenReturn(listaMed);
        when(medicamentoService.getPagesGetMedicamentos(Long.valueOf(1), "pacienteDNI")).thenReturn(Long.valueOf(1));
        when(paginationResponseFactory.newPaginationResponse(any(),any())).thenReturn(paginationResponse);

        mockMvc
            .perform(get(String.format("/getMedicamentos/%s/%d/%d/%d"
                    ,"pacienteDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data", hasSize(2)))
            .andExpect(jsonPath("$.metadata.pageNumber", is(1)))
            .andExpect(jsonPath("$.metadata.pageSize", is(1)))
            .andExpect(jsonPath("$.metadata.totalPages", is(1)))
            .andExpect(jsonPath("$.data[0]", is("Medicamento1")))
            .andExpect(jsonPath("$.data[1]", is("Medicamento2")));
        }

        @Test
        public void getMedicamentos_whenCallingWithPacienteAndPageInRange_shouldReturnPage() throws Exception {
            List<String> listaMed=List.of("Medicamento1", "Medicamento2");
            PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
            PaginationResponse paginationResponse =new PaginationResponse(listaMed,pageMetadata);
            int tokenUsuario=1;
            when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
            when(tokenizacionDAO.getDNI(tokenUsuario)).thenReturn("dummyDNI");
            when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));
            when(pageMetadataFactory.newPageMetadata(anyLong(),anyLong(),anyLong())).thenReturn(pageMetadata);
            when(medicamentoDAO.isNumPageInRange(any())).thenReturn(true);
            when(medicamentoService.getMedicamentos(any(),anyString())).thenReturn(listaMed);
            when(medicamentoService.getPagesGetMedicamentos(Long.valueOf(1), "pacienteDNI")).thenReturn(Long.valueOf(1));
            when(paginationResponseFactory.newPaginationResponse(any(),any())).thenReturn(paginationResponse);

            mockMvc
                .perform(get(String.format("/getMedicamentos/%s/%d/%d/%d"
                    ,"dummyDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data", hasSize(2)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)))
                .andExpect(jsonPath("$.metadata.pageSize", is(1)))
                .andExpect(jsonPath("$.metadata.totalPages", is(1)))
                .andExpect(jsonPath("$.data[0]", is("Medicamento1")))
                .andExpect(jsonPath("$.data[1]", is("Medicamento2")));
        }

    @Test
    public void getMedicamentos_whenCallingWithNoPermissionObserver_shouldReturnUnauthorized() throws Exception{
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;

        when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(tokenizacionDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
        when(userService.getRoles(anyString())).thenReturn(List.of());
        when(medicamentoService.getPagesGetMedicamentos(Long.valueOf(1), "pacienteDNI")).thenReturn(Long.valueOf(1));

        mockMvc
                .perform(get(String.format("/getMedicamentos/%s/%d/%d/%d"
                    ,"pacienteDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void getMedicamentos_whenCallingWithValidObserverAndPageOutOfRange_shouldReturnBadRequest() throws Exception{
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;
        when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
	    when(medicamentoService.getPagesGetMedicamentos(any(Long.class), anyString())).thenReturn((long)20);
    	when(tokenizacionDAO.getDNI(any(Integer.class))).thenReturn("dummyDoctorDNI");
    	when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
    	when(medicamentoDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

    	mockMvc
            .perform(get(String.format("/getMedicamentos/%s/%d/%d/%d"
                    ,"pacienteDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void getMedicamentos_whenCallingWithWrongPaciente_shouldReturnUnauthorized() throws Exception{
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;
        when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(tokenizacionDAO.getDNI(any(Integer.class))).thenReturn("paciente2DNI");
    	when(medicamentoService.getPagesGetMedicamentos(any(Long.class), anyString())).thenReturn((long)1);
    	when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));

    	mockMvc
            .perform(get(String.format("/getMedicamentos/%s/%d/%d/%d"
                    ,"paciente1DNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void getMedicamentos_whenCallingWithNonExistingTokenInTokenizacion_shouldReturnNoContent() throws Exception {
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;
        when(medicamentoService.getPagesGetMedicamentos(any(Long.class), anyString())).thenReturn((long)1);
        when(tokenizacionDAO.getDNI(any(Integer.class))).thenReturn(null);

        mockMvc
            .perform(get(String.format("/getMedicamentos/%s/%d/%d/%d"
                    ,"pacienteDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isNoContent());
    }

    @Test
    public void getMedicamentos_whenCallingWithNonExistingUser_shouldReturnNoContent() throws Exception {
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;
        when(medicamentoService.getPagesGetMedicamentos(any(Long.class), anyString())).thenReturn((long)0);

        mockMvc
            .perform(get(String.format("/getMedicamentos/%s/%d/%d/%d"
                    ,"dummyDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isNoContent());
    }

    @Test
    public void getInfoMedicamento_whenCallingWithExistingMedicamentoAndPageInRange_shouldReturnPage() throws Exception {
        Medicamento med1 = new Medicamento(1,"med1","contraind1","pos1","prosp1","comp1");
        
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));

        MedicamentoResponse medResponse1 = new MedicamentoResponse(1,"med1","contraind1","pos1","prosp1","comp1","url1");
        MedicamentoResponse medResponse2 = new MedicamentoResponse(2,"med2","contraind2","pos2","prosp2","comp2","url2");
        List<MedicamentoResponse> listaResponse = List.of(medResponse1, medResponse2);
        PaginationResponse paginationResponse =new PaginationResponse(listaResponse,pageMetadata);
        
        when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(paginationResponseFactory.newPaginationResponse(any(),any())).thenReturn(paginationResponse);
        when(medicamentoService.getInfoMedicamento(pageMetadata, med1.getNombre())).thenReturn(listaResponse);
        when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(pageMetadataFactory.newPageMetadata(anyLong(),anyLong(),anyLong())).thenReturn(pageMetadata);
        when(medicamentoDAO.isNumPageInRange(any())).thenReturn(true);

        mockMvc
                .perform(get(String.format("/getInfoMedicamento?nombre=%s&pageSize=%d&pageNumber=%d", "med1", 
                    pageMetadata.getPageSize(), pageMetadata.getPageNumber())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data", hasSize(2)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)))
                .andExpect(jsonPath("$.metadata.pageSize", is(1)))
                .andExpect(jsonPath("$.metadata.totalPages", is(1)))
                .andExpect(jsonPath("$.data[0].idMedicamento", is(medResponse1.getIdMedicamento())))
                .andExpect(jsonPath("$.data[0].url", is(medResponse1.getURL())))
                .andExpect(jsonPath("$.data[0].nombre", is(medResponse1.getNombre())))
                .andExpect(jsonPath("$.data[0].contraindicaciones", is(medResponse1.getContraindicaciones())))
                .andExpect(jsonPath("$.data[0].composicion", is(medResponse1.getComposicion())))
                .andExpect(jsonPath("$.data[0].prospecto", is(medResponse1.getProspecto())))
                .andExpect(jsonPath("$.data[0].posologia", is(medResponse1.getPosologia())))
                .andExpect(jsonPath("$.data[1].idMedicamento", is(medResponse2.getIdMedicamento())))
                .andExpect(jsonPath("$.data[1].url", is(medResponse2.getURL())))
                .andExpect(jsonPath("$.data[1].nombre", is(medResponse2.getNombre())))
                .andExpect(jsonPath("$.data[1].contraindicaciones", is(medResponse2.getContraindicaciones())))
                .andExpect(jsonPath("$.data[1].composicion", is(medResponse2.getComposicion())))
                .andExpect(jsonPath("$.data[1].prospecto", is(medResponse2.getProspecto())))
                .andExpect(jsonPath("$.data[1].posologia", is(medResponse2.getPosologia())));
    }

    @Test
    public void getInfoMedicamento_whenPageOutOfRange_shouldReturnBadRequest() throws Exception {
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));

        when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
    	when(medicamentoDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

    	mockMvc
        .perform(get(String.format("/getInfoMedicamento?nombre=%s&pageSize=%d&pageNumber=%d", "medicamento", 
            pageMetadata.getPageSize(), pageMetadata.getPageNumber())))
        .andExpect(status().isBadRequest());
    }

    @Test
    public void getInfoMedicamento_whenCallingWithNonExistingMedicamento_shouldReturnNoContent() throws Exception {
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));

        when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(0));

        mockMvc
        .perform(get(String.format("/getInfoMedicamento?nombre=%s&pageSize=%d&pageNumber=%d", "medicamento", 
            pageMetadata.getPageSize(), pageMetadata.getPageNumber())))
        .andExpect(status().isNoContent());
    }

    @Test
    public void getInfoMedicamento_whenPageSizeZero_shouldReturnBadRequest() throws Exception {
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(0),Long.valueOf(1),Long.valueOf(1));

        when(medicamentoDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
    	when(medicamentoDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

    	mockMvc
        .perform(get(String.format("/getInfoMedicamento?nombre=%s&pageSize=%d&pageNumber=%d", "medicamento", 
            pageMetadata.getPageSize(), pageMetadata.getPageNumber())))
        .andExpect(status().isBadRequest());
    }
}