package app.controller;

import app.dao.TokenizacionDAO;
import app.dao.UserDAO;
import app.dao.ConsultaDAO;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import app.model.User;
import app.model.Consulta;
import app.model.response.ConsultaDetallesResponse;
import app.model.response.ConsultaResponse;
import app.model.response.PageMetadata;
import app.service.ConsultaService;
import app.service.UserService;
import java.util.List;
import java.sql.Timestamp;
import java.time.Instant;

@RunWith(SpringRunner.class)
@WebMvcTest(ConsultaController.class)
public class ConsultaControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ConsultaDAO consultaDAO;
  @MockBean
  private TokenizacionDAO tokDAO;
  @MockBean
  private UserService userService;
  @MockBean
  private ConsultaService consultaService;
  @MockBean
  private UserDAO userDAO;

  @Test
  public void getConsultas_whenCalledWithBothFilters_shouldReturnBadRequest() throws Exception{
    mockMvc.perform(get("/getConsultas/dummyDNI/1/123456789/dummyDoctorNombreFiltro/0/0"))
    .andExpect(status().isBadRequest());
  }

  @Test
  public void getConsultas_whenCallingWithNonExistingUserInConsulta_shouldReturnNoContent() throws Exception {
    when(consultaService.getPagesGetConsultas(
      any(Long.class), anyString(), any(Long.class), anyString()))
    .thenReturn((long)0);

    mockMvc.perform(get("/getConsultas/dummyDNI/1/123456789/0/0/0"))
    .andExpect(status().isNoContent());
  }
  
  @Test
  public void getConsultas_whenCallingWithNonExistingTokenInTokenizacion_shouldReturnNoContent() throws Exception {
    when(consultaService.getPagesGetConsultas(
      any(Long.class), anyString(), any(Long.class), anyString()))
    .thenReturn((long)1);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn(null);

    mockMvc.perform(get("/getConsultas/dummyDNI/1/0/dummyDoctorNombreFiltro/0/0"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getConsultas_whenCallingWithNoPermissionObserver_shouldReturnUnauthorized() throws Exception{
    when(consultaService.getPagesGetConsultas(
      any(Long.class), anyString(), any(Long.class), anyString()))
    .thenReturn((long)1);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of());

    mockMvc.perform(get("/getConsultas/dummyDNI/1/0/0/0/0"))
    .andExpect(status().is(401)); //Unathorized
  }

  @Test
  public void getConsultas_whenCallingWithWrongPaciente_shouldReturnUnauthorized() throws Exception{
    when(consultaService.getPagesGetConsultas(
      any(Long.class), anyString(), any(Long.class), anyString()))
    .thenReturn((long)1);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDNI2");
    when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));

    mockMvc.perform(get("/getConsultas/dummyDNI/0/0/0/0/0"))
    .andExpect(status().is(401)); //Unathorized
  }

  @Test
  public void getConsultas_whenCallingWithValidObserverAndPageOutOfRange_shouldReturnBadRequest() throws Exception{
    when(consultaService.getPagesGetConsultas(
      any(Long.class), anyString(), any(Long.class), anyString()))
    .thenReturn((long)20);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDoctorDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
    when(consultaDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

    mockMvc.perform(get("/getConsultas/dummyDNI/23/0/0/1/30"))
    .andExpect(status().isBadRequest());
  }

  @Test
  public void getConsultas_whenCallingWithSamePacienteAndPageInRange_shouldReturnPage() throws Exception {
    ConsultaResponse cr = new ConsultaResponse(
      1, Timestamp.from(Instant.now()), "dummyDoctorNombre");
    List<ConsultaResponse> l = List.of(cr);
   
    when(consultaService.getPagesGetConsultas(
      any(Long.class), anyString(), any(Long.class), anyString()))
    .thenReturn((long)20);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));
    when(consultaDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(true);

    when(consultaService.getConsultas(
      any(Long.class), anyString(), any(PageMetadata.class), anyString())).thenReturn(l);

    mockMvc.perform(get("/getConsultas/dummyDNI/23/0/0/1/17"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.metadata.pageSize", is(1)))
    .andExpect(jsonPath("$.metadata.pageNumber", is(17)))
    .andExpect(jsonPath("$.metadata.totalPages", is(20)))
    .andExpect(jsonPath("$.data[0].idConsulta", is(cr.getIdConsulta())))
    .andExpect(jsonPath("$.data[0].fechaRealizacion", is(
        cr.getFechaRealizacion().toInstant().toEpochMilli())));
  }

  //Todo ok
  @Test
  public void getConsultaDetalles_whenCallingWhithRightIdAndToken() throws Exception {
    Timestamp ts = new Timestamp(System.currentTimeMillis()-40000);
    Consulta consulta = new Consulta(
      "dummyPacienteDNI", "dummyDrDNI", "Observacion", 123, ts);
    ConsultaDetallesResponse consultaDetalles =new ConsultaDetallesResponse(
      "dummyPacienteDNI", ts, "Observacion", "name");

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    User u = new User();
    u.setNombre("name");
    when(userDAO.getUser(anyString())).thenReturn(u);
    when(consultaDAO.getConsulta(anyInt())).thenReturn(consulta);
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
    mockMvc.perform(get("/getConsultaDetalles/123/1"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.pacienteDNI", is(consultaDetalles.getPacienteDNI())))
    .andExpect(jsonPath("$.observaciones", is(consultaDetalles.getObservaciones())))
    .andExpect(jsonPath("$.doctorNombre", is(consultaDetalles.getDoctorNombre())))
    .andExpect(jsonPath("$.fechaRealizacion", 
      is(consultaDetalles.getFechaRealizacion().toInstant().toEpochMilli())));


  }
  @Test
  //Error 204 nocontent
  public void getConsultaDetalles_withNonExistingIdOrToken() throws Exception {
    when(tokDAO.getToken(anyString())).thenReturn(null);
    when(consultaDAO.getConsulta(any(Integer.class))).thenReturn(new Consulta());

    mockMvc.perform(get("/getConsultaDetalles/123/1")) //Aqui el id no está
    .andExpect(status().isNoContent()); //Error 204
  }
  @Test
  //Error 401
  public void getConsultasDetalles_whenCallingWithWrongToken_shouldReturnUnauthorized() throws Exception{
    Timestamp ts = new Timestamp(System.currentTimeMillis()-40000);
    Consulta consulta = new Consulta(
      "dummyPacienteDNI", "dummyDrDNI", "Observacion", 123, ts);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDifferentPacienteDNI");
    User u = new User();
    u.setNombre("name");
    when(userDAO.getUser(anyString())).thenReturn(u);
    when(consultaDAO.getConsulta(anyInt())).thenReturn(consulta);
    when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));

    /*when(tokDAO.getToken(anyString())).thenReturn(1);
    when(consultaDAO.getConsulta(123)).thenReturn(consulta);*/
    mockMvc.perform(get("/getConsultaDetalles/345/2"))
    .andExpect(status().is(401)); //Unathorized
  }

}
