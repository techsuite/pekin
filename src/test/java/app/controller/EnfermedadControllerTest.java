package app.controller;

import app.dao.EnfermedadDAO;
import app.dao.TokenizacionDAO;
import app.factory.PageMetadataFactory;
import app.factory.PaginationResponseFactory;
import app.model.Enfermedad;
import app.model.response.EnfermedadResponse;
import app.model.response.PageMetadata;
import app.model.response.PaginationResponse;
import app.service.EnfermedadService;
import app.service.UserService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(EnfermedadController.class)
public class EnfermedadControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EnfermedadDAO enfermedadDAO;
    @MockBean
    private EnfermedadService enfermedadService;
    @MockBean
    private TokenizacionDAO tokenizacionDAO;
    @MockBean
    private UserService userService;
    @MockBean
    private PageMetadataFactory pageMetadataFactory;
    @MockBean
    private PaginationResponseFactory paginationResponseFactory;

    @Test
    public void getEnfermedades_whenCallingWithDoctorAndPageInRange_shouldReturnPage() throws Exception {
        List<String> listaEnf=List.of("enfermedad1", "enfermedad2"); 
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        PaginationResponse paginationResponse =new PaginationResponse(listaEnf,pageMetadata);
        int tokenUsuario=1;
        when(enfermedadDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(tokenizacionDAO.getDNI(tokenUsuario)).thenReturn("dniObservador1");
        when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
        when(pageMetadataFactory.newPageMetadata(anyLong(),anyLong(),anyLong())).thenReturn(pageMetadata);
        when(enfermedadDAO.isNumPageInRange(any())).thenReturn(true);
        when(enfermedadService.getEnfermedades(any(),anyString())).thenReturn(listaEnf);
        when(enfermedadService.getPagesGetEnfermedades(Long.valueOf(1), "pacienteDNI")).thenReturn(Long.valueOf(1));
        when(paginationResponseFactory.newPaginationResponse(any(),any())).thenReturn(paginationResponse);

        mockMvc
            .perform(get(String.format("/getEnfermedades/%s/%d/%d/%d"
                    ,"pacienteDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.data", hasSize(2)))
            .andExpect(jsonPath("$.metadata.pageNumber", is(1)))
            .andExpect(jsonPath("$.metadata.pageSize", is(1)))
            .andExpect(jsonPath("$.metadata.totalPages", is(1)))
            .andExpect(jsonPath("$.data[0]", is("enfermedad1")))
            .andExpect(jsonPath("$.data[1]", is("enfermedad2")));
        }

        @Test
        public void getEnfermedades_whenCallingWithPacienteAndPageInRange_shouldReturnPage() throws Exception {
            List<String> listaEnf=List.of("enfermedad1", "enfermedad2");             
            PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
            PaginationResponse paginationResponse =new PaginationResponse(listaEnf,pageMetadata);
            int tokenUsuario=1;
            when(enfermedadDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
            when(tokenizacionDAO.getDNI(tokenUsuario)).thenReturn("dummyDNI");
            when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));
            when(pageMetadataFactory.newPageMetadata(anyLong(),anyLong(),anyLong())).thenReturn(pageMetadata);
            when(enfermedadDAO.isNumPageInRange(any())).thenReturn(true);
            when(enfermedadService.getEnfermedades(any(),anyString())).thenReturn(listaEnf);
            when(enfermedadService.getPagesGetEnfermedades(Long.valueOf(1), "dummyDNI")).thenReturn(Long.valueOf(1));
            when(paginationResponseFactory.newPaginationResponse(any(),any())).thenReturn(paginationResponse);

            mockMvc
                .perform(get(String.format("/getEnfermedades/%s/%d/%d/%d"
                    ,"dummyDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data", hasSize(2)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)))
                .andExpect(jsonPath("$.metadata.pageSize", is(1)))
                .andExpect(jsonPath("$.metadata.totalPages", is(1)))
                .andExpect(jsonPath("$.data[0]", is("enfermedad1")))
                .andExpect(jsonPath("$.data[1]", is("enfermedad2")));
        }

    @Test
    public void getEnfermedades_whenCallingWithNoPermissionObserver_shouldReturnUnauthorized() throws Exception{
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;

        when(enfermedadDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(tokenizacionDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
        when(userService.getRoles(anyString())).thenReturn(List.of());
        when(enfermedadService.getPagesGetEnfermedades(Long.valueOf(1), "pacienteDNI")).thenReturn(Long.valueOf(1));

        mockMvc
                .perform(get(String.format("/getEnfermedades/%s/%d/%d/%d"
                    ,"pacienteDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void getEnfermedades_whenCallingWithValidObserverAndPageOutOfRange_shouldReturnBadRequest() throws Exception{
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;
        when(enfermedadDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
	    when(enfermedadService.getPagesGetEnfermedades(any(Long.class), anyString())).thenReturn((long)20);
    	when(tokenizacionDAO.getDNI(any(Integer.class))).thenReturn("dummyDoctorDNI");
    	when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
    	when(enfermedadDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

    	mockMvc
            .perform(get(String.format("/getEnfermedades/%s/%d/%d/%d"
                    ,"pacienteDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void getPEnfermedades_whenCallingWithWrongPaciente_shouldReturnUnauthorized() throws Exception{
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;
        when(enfermedadDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(tokenizacionDAO.getDNI(any(Integer.class))).thenReturn("paciente2DNI");
    	when(enfermedadService.getPagesGetEnfermedades(any(Long.class), anyString())).thenReturn((long)1);
    	when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));

    	mockMvc
            .perform(get(String.format("/getEnfermedades/%s/%d/%d/%d"
                    ,"paciente1DNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void getEnfermedades_whenCallingWithNonExistingTokenInTokenizacion_shouldReturnNoContent() throws Exception {
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;
        when(enfermedadService.getPagesGetEnfermedades(any(Long.class), anyString())).thenReturn((long)1);
        when(tokenizacionDAO.getDNI(any(Integer.class))).thenReturn(null);

        mockMvc
            .perform(get(String.format("/getEnfermedades/%s/%d/%d/%d"
                    ,"pacienteDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isNoContent());
    }

    @Test
    public void getEnfermedades_whenCallingWithNonExistingUserInPrueba_shouldReturnNoContent() throws Exception {
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));
        int tokenUsuario=1;
        when(enfermedadService.getPagesGetEnfermedades(any(Long.class), anyString())).thenReturn((long)0);

        mockMvc
            .perform(get(String.format("/getEnfermedades/%s/%d/%d/%d"
                    ,"dummyDNI",tokenUsuario,pageMetadata.getPageSize(),pageMetadata.getPageNumber())))
            .andExpect(status().isNoContent());
    }


    @Test
    public void getInfoEnfermedad_whenCallingWithExistingEnfermedadAndPageInRange_shouldReturnPage() throws Exception {
        Enfermedad enf1 = new Enfermedad(1,"enf1","sint1","recom1");
        
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));

        EnfermedadResponse enfResponse1 = new EnfermedadResponse(1,"enf1","sint1","recom1","url1");
        List<EnfermedadResponse> listaResponse = List.of(enfResponse1);
        PaginationResponse paginationResponse =new PaginationResponse(listaResponse,pageMetadata);
        
        when(enfermedadDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(paginationResponseFactory.newPaginationResponse(any(),any())).thenReturn(paginationResponse);
        when(enfermedadService.getInfoEnfermedad(pageMetadata, enf1.getNombre())).thenReturn(listaResponse);
        when(enfermedadDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
        when(pageMetadataFactory.newPageMetadata(anyLong(),anyLong(),anyLong())).thenReturn(pageMetadata);
        when(enfermedadDAO.isNumPageInRange(any())).thenReturn(true);

        mockMvc
                .perform(get(String.format("/getInfoEnfermedad?nombre=%s&pageSize=%d&pageNumber=%d", "enf1", 
                    pageMetadata.getPageSize(), pageMetadata.getPageNumber())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data", hasSize(1)))
                .andExpect(jsonPath("$.metadata.pageNumber", is(1)))
                .andExpect(jsonPath("$.metadata.pageSize", is(1)))
                .andExpect(jsonPath("$.metadata.totalPages", is(1)))
                .andExpect(jsonPath("$.data[0].idEnfermedad", is(enfResponse1.getIdEnfermedad())))
                .andExpect(jsonPath("$.data[0].nombre", is(enfResponse1.getNombre())))
                .andExpect(jsonPath("$.data[0].sintomas", is(enfResponse1.getSintomas())))
                .andExpect(jsonPath("$.data[0].recomendaciones", is(enfResponse1.getRecomendaciones())))
                .andExpect(jsonPath("$.data[0].url", is(enfResponse1.getUrl())));

    }


    @Test
    public void getInfoEnfermedad_whenPageOutOfRange_shouldReturnBadRequest() throws Exception {
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));

        when(enfermedadDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(1));
    	when(enfermedadDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

    	mockMvc
        .perform(get(String.format("/getInfoEnfermedad?nombre=%s&pageSize=%d&pageNumber=%d", "enfermedad", 
            pageMetadata.getPageSize(), pageMetadata.getPageNumber())))
        .andExpect(status().isBadRequest());
    }

    @Test
    public void getInfoEnfermedad_whenCallingWithNonExistingEnfermedad_shouldReturnNoContent() throws Exception {
        PageMetadata pageMetadata = new PageMetadata(Long.valueOf(1),Long.valueOf(1),Long.valueOf(1));

        when(enfermedadDAO.getNumPages(anyString(), any(), any())).thenReturn(Long.valueOf(0));

        mockMvc
        .perform(get(String.format("/getInfoEnfermedad?nombre=%s&pageSize=%d&pageNumber=%d", "enfermedad", 
            pageMetadata.getPageSize(), pageMetadata.getPageNumber())))
        .andExpect(status().isNoContent());
    }





}
