package app.controller;

import app.dao.TokenizacionDAO;
import app.dao.UserDAO;
import app.dao.UsuarioImpostorDAO;
import java.time.LocalDate;
import java.sql.Date;
import java.time.Period;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import com.google.gson.Gson;
import app.model.User;
import app.service.UserService;
import java.util.List;
import java.util.ArrayList;
import app.model.UsuarioImpostor;
import app.model.response.UsuarioImpostorResponse;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private UserDAO userDAO;
  @MockBean
  private TokenizacionDAO tokDAO;
  @MockBean
  private UserService userService;

  @MockBean
  private UsuarioImpostorDAO usuarioImpostorDAO;

  @Test
  public void getLoginStatus_whenCallingWithNonExistingUser_shouldReturnNoContent() throws Exception {
    when(userDAO.getUser(anyString())).thenReturn(null);
    mockMvc.perform(get("/loginStatus/222/dummyPasswd"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getLoginStatus_whenCallingWithCorrectUserData_shouldReturnToken() throws Exception {
    User u = new User();
    u.setContrasena("jg2000");

    when(tokDAO.getToken(anyString())).thenReturn(1);
    when(userDAO.getUser(anyString())).thenReturn(u);
    mockMvc.perform(get("/loginStatus/02565323J/jg2000"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.token", is(1)));
  }

  @Test
  public void getLoginStatus_whenCallingWithIncorrectPassword_shouldReturnNoContent() throws Exception {
    User u = new User();
    u.setContrasena("jg2000");

    when(tokDAO.getToken(anyString())).thenReturn(1);
    when(userDAO.getUser(anyString())).thenReturn(u);
    mockMvc.perform(get("/loginStatus/02565323J/dummyPasswd"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getAtributos_whenCallingWithNonExistingToken_shouldReturnNoContent() throws Exception {
    when(tokDAO.getDNI(any(Integer.class))).thenReturn(null);
    mockMvc.perform(get("/user/-1"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getAtributos_whenCallingWithExistingToken_shouldReturnUserResponse() throws Exception {
    Date dummyDate = Date.valueOf("1996-7-9");
    User u = new User("dummyDNI", "dummyPasswd", "Javier", "de Andrés González", "Varon", 
      "javier.dandresg@alumnos.upm.es", dummyDate);

    Integer edad = Period.between(dummyDate.toLocalDate(), LocalDate.now()).getYears();

    when(tokDAO.getDNI(any(Integer.class))).thenReturn(u.getDNI());
    when(userDAO.getUser(u.getDNI())).thenReturn(u);
    when(userService.getEdad(anyString())).thenReturn(edad);

    mockMvc.perform(get("/user/1"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.nombre", is(u.getNombre())))
    .andExpect(jsonPath("$.apellidos", is(u.getApellidos())))
    .andExpect(jsonPath("$.genero", is(u.getGenero())))
    .andExpect(jsonPath("$.correo_electronico", is(u.getCorreo_electronico())))
    .andExpect(jsonPath("$.edad", is(edad)));
  }

  @Test
  public void getRoles_whenCallingWithNonExistingToken_shouldReturnNoContent() throws Exception {
    when(tokDAO.getDNI(any(Integer.class))).thenReturn(null);
    mockMvc.perform(get("/getRoles/-1"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getRoles_whenCallingWithExistingToken_shouldReturnNoContent() throws Exception {
    List<String> roles = new ArrayList<>();
    roles.add("Doctor");
    roles.add("TrabajadorLaboratorio");

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDNI");
    when(userService.getRoles(anyString())).thenReturn(roles);

    String outputBody = mockMvc.perform(get("/getRoles/1"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$[*]", hasSize(roles.size())))
    .andReturn()
    .getResponse()
    .getContentAsString();

    String outputRoles[] = new Gson().fromJson(outputBody, String[].class);
    
    int i = 0;
    for(String rol : outputRoles){
      Assert.assertEquals(roles.get(i++), rol);
    }
  }

  @Test
  public void getDNIUsuarioASuplantar_whenCallingWithNonExistingTokenOrNonMatchingImpostorDNI_shouldReturnNoContent() throws Exception {
    when(usuarioImpostorDAO.getUsuarioImpostor(anyString(), any(Integer.class))).thenReturn(null);
    mockMvc.perform(get("/loginStatus/impostor/dummyDNI/0"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getDNIUsuarioASuplantar_whenCallingWithCorrectTokenAndImpostorDNI_shouldReturnUsuarioDNI() throws Exception {
    UsuarioImpostor u = new UsuarioImpostor("dummyUsuarioDNI", "dummyImpostorDNI", 427);
    UsuarioImpostorResponse ur = new UsuarioImpostorResponse(1);

    when(usuarioImpostorDAO.getUsuarioImpostor(anyString(), any(Integer.class))).thenReturn(u);
    when(usuarioImpostorDAO.getTokenUsuarioSuplantado(any(UsuarioImpostor.class))).thenReturn(ur);
    
    mockMvc.perform(get("/loginStatus/impostor/dummyImpostorDNI/427"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.token", is(ur.getToken())));
  }

}
