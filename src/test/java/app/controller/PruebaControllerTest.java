package app.controller;

import app.dao.TokenizacionDAO;
import app.dao.PruebaDAO;
import app.dao.PCRDAO;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import app.model.response.PruebaResponse;
import app.model.response.AnalisisSangreDetallesResponse;
import app.model.response.PCRDetallesResponse;
import app.model.response.PageMetadata;
import app.model.Prueba;
import app.model.PCR;
import app.service.PruebaService;
import app.service.UserService;
import java.util.List;
import java.sql.Timestamp;
import java.time.Instant;

@RunWith(SpringRunner.class)
@WebMvcTest(PruebaController.class)
public class PruebaControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private PruebaDAO pruebaDAO;
  @MockBean
  private TokenizacionDAO tokDAO;
  @MockBean
  private UserService userService;
  @MockBean
  private PruebaService pruebaService; 
  @MockBean
  private PCRDAO pcrDAO;

  @Test
  public void getPruebaDetalles_whenCallingWithNonExistentIdPrueba_shouldReturnNoContent() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(pruebaDAO.getPrueba(any(Integer.class))).thenReturn(null);

    mockMvc.perform(get("/getPruebaDetalles/0/0"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getPruebaDetalles_whenCallingWithNonExistentObserverDNI_shouldReturnNoContent() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn(null);
    when(pruebaDAO.getPrueba(any(Integer.class))).thenReturn(new Prueba());

    mockMvc.perform(get("/getPruebaDetalles/0/0"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getPruebaDetalles_whenCallingWithNoDoctorNoSamePacienteToken_shouldReturnUnauthorized() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(pruebaDAO.getPrueba(any(Integer.class))).thenReturn(new Prueba());
    when(userService.getRoles(anyString())).thenReturn(List.of());

    mockMvc.perform(get("/getPruebaDetalles/0/0"))
    .andExpect(status().is(401)); //Unauthorized
  }

  @Test
  public void getPruebaDetalles_whenCallingWithDifferentPacienteToken_shouldReturnUnauthorized() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDNI");
    Prueba p = new Prueba();
    p.setPacienteDNI("differentDummyDNI");
    when(pruebaDAO.getPrueba(any(Integer.class))).thenReturn(p);
    when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));

    mockMvc.perform(get("/getPruebaDetalles/0/0"))
    .andExpect(status().is(401)); //Unauthorized
  }

  @Test
  public void getPruebaDetalles_whenCallingWithDoctorTokenAndPCR_shouldReturnCorrectly() throws Exception{
    PCRDetallesResponse pcrResp = new PCRDetallesResponse();
    pcrResp.setTrabLabNombre("dummyTrabLabNombre");
    pcrResp.setFechaRealizacion(Timestamp.from(Instant.now()));
    pcrResp.setTipoPrueba("PCR");
    pcrResp.setPacienteDNI("dummyPacienteDNI");
    pcrResp.setFluorescencia(0.452);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(pruebaDAO.getPrueba(any(Integer.class))).thenReturn(new Prueba());
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
    when(pcrDAO.getPCR(any(Integer.class))).thenReturn(new PCR());
    when(pruebaDAO.getPCRDetalles(any(Integer.class))).thenReturn(pcrResp);

    mockMvc.perform(get("/getPruebaDetalles/0/0"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.trabLabNombre", is(pcrResp.getTrabLabNombre())))
    .andExpect(jsonPath("$.fechaRealizacion", 
      is(pcrResp.getFechaRealizacion().toInstant().toEpochMilli())))
    .andExpect(jsonPath("$.tipoPrueba", is(pcrResp.getTipoPrueba())))
    .andExpect(jsonPath("$.pacienteDNI", is(pcrResp.getPacienteDNI())))
    .andExpect(jsonPath("$.fluorescencia", is(pcrResp.getFluorescencia())));
  }

  @Test
  public void 
      getPruebaDetalles_whenCallingWithSamePacienteTokenAndAnalisisSangre_shouldReturnCorrectly() 
      throws Exception{

    AnalisisSangreDetallesResponse anSangResp = new AnalisisSangreDetallesResponse();
    anSangResp.setTrabLabNombre("dummyTrabLabNombre");
    anSangResp.setFechaRealizacion(Timestamp.from(Instant.now()));
    anSangResp.setTipoPrueba("AnalisisSangre");
    anSangResp.setPacienteDNI("dummyPacienteDNI");
    anSangResp.setBilirrubina(0.452);
    anSangResp.setColesterol(321.1);
    anSangResp.setElectrolitos(7.543);
    anSangResp.setGlucosa(1.12121);
    anSangResp.setTrigliceridos(856.3);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDNI");
    Prueba p = new Prueba();
    p.setPacienteDNI("dummyDNI");
    when(pruebaDAO.getPrueba(any(Integer.class))).thenReturn(p);
    when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));
    when(pcrDAO.getPCR(any(Integer.class))).thenReturn(null);
    when(pruebaDAO.getAnalisisSangreDetalles(any(Integer.class))).thenReturn(anSangResp);

    mockMvc.perform(get("/getPruebaDetalles/0/0"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.trabLabNombre", is(anSangResp.getTrabLabNombre())))
    .andExpect(jsonPath("$.fechaRealizacion", 
      is(anSangResp.getFechaRealizacion().toInstant().toEpochMilli())))
    .andExpect(jsonPath("$.tipoPrueba", is(anSangResp.getTipoPrueba())))
    .andExpect(jsonPath("$.pacienteDNI", is(anSangResp.getPacienteDNI())))
    .andExpect(jsonPath("$.bilirrubina", is(anSangResp.getBilirrubina())))
    .andExpect(jsonPath("$.colesterol", is(anSangResp.getColesterol())))
    .andExpect(jsonPath("$.electrolitos", is(anSangResp.getElectrolitos())))
    .andExpect(jsonPath("$.glucosa", is(anSangResp.getGlucosa())))
    .andExpect(jsonPath("$.trigliceridos", is(anSangResp.getTrigliceridos())));
  }

  @Test
  public void getPruebas_whenCalledWithMoreThanOneFilter_shouldReturnBadRequest() throws Exception{
    mockMvc.perform(get("/getPruebas/dummyDNI/0/1234/0/dummyTrabLabNombreFiltro/0/0"))
    .andExpect(status().isBadRequest());
    
    mockMvc.perform(get("/getPruebas/dummyDNI/0/1234/dummyTipoPruebaFiltro/0/0/0"))
    .andExpect(status().isBadRequest());
    
    mockMvc.perform(get("/getPruebas/dummyDNI/0/0/dummyTipoPruebaFiltro/dummyTrabLabNombreFiltro/0/0"))
    .andExpect(status().isBadRequest());
  }

  @Test
  public void getPruebas_whenCalledWithTipoPruebaFilterNoPCRNoAnalisisSangre_shouldReturnBadRequest() throws Exception{
    mockMvc.perform(get("/getPruebas/dummyDNI/0/0/badTipoPruebaFiltro/0/0/0"))
    .andExpect(status().isBadRequest());
  }

  @Test
  public void getPruebas_whenCallingWithNonExistingUserInPrueba_shouldReturnNoContent() throws Exception {
    when(pruebaService.getPagesGetPruebas(
      any(Long.class), anyString(), anyString(), any(Long.class), anyString()))
    .thenReturn((long)0);

    mockMvc.perform(get("/getPruebas/dummyDNI/0/0/0/dummyTrabLabNombreFiltro/0/0"))
    .andExpect(status().isNoContent());
  }
  
  @Test
  public void getPruebas_whenCallingWithNonExistingTokenInTokenizacion_shouldReturnNoContent() throws Exception {
    when(pruebaService.getPagesGetPruebas(
      any(Long.class), anyString(), anyString(), any(Long.class), anyString()))
    .thenReturn((long)1);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn(null);

    mockMvc.perform(get("/getPruebas/dummyDNI/0/0/PCR/0/0/0"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getPruebas_whenCallingWithNoPermissionObserver_shouldReturnUnauthorized() throws Exception{
    when(pruebaService.getPagesGetPruebas(
      any(Long.class), anyString(), anyString(), any(Long.class), anyString()))
    .thenReturn((long)1);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of());

    mockMvc.perform(get("/getPruebas/dummyDNI/0/0/AnalisisSangre/0/0/0"))
    .andExpect(status().is(401)); //Unathorized
  }

  @Test
  public void getPruebas_whenCallingWithWrongPaciente_shouldReturnUnauthorized() throws Exception{
    when(pruebaService.getPagesGetPruebas(
      any(Long.class), anyString(), anyString(), any(Long.class), anyString()))
    .thenReturn((long)1);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDNI2");
    when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));

    mockMvc.perform(get("/getPruebas/dummyDNI/0/1234/0/0/0/0"))
    .andExpect(status().is(401)); //Unathorized
  }

  @Test
  public void getPruebas_whenCallingWithValidObserverAndPageOutOfRange_shouldReturnBadRequest() throws Exception{
    when(pruebaService.getPagesGetPruebas(
      any(Long.class), anyString(), anyString(), any(Long.class), anyString()))
    .thenReturn((long)20);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDoctorDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
    when(pruebaDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

    mockMvc.perform(get("/getPruebas/dummyDNI/1/1234/0/0/1/30"))
    .andExpect(status().isBadRequest());
  }

  @Test
  public void getPruebas_whenCallingWithSamePacienteAndPageInRangeAndAnalisisSangre_shouldReturnPage() throws Exception {
    PruebaResponse pr = new PruebaResponse(
        1, Timestamp.from(Instant.now()), 
        "AnalisisSangre", "dummyTrabLabNombre");

    List<PruebaResponse> l = List.of(pr);
   
    when(pruebaService.getPagesGetPruebas(
      any(Long.class), anyString(), anyString(), any(Long.class), anyString()))
    .thenReturn((long)20);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Paciente"));
    when(pruebaDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(true);
   
    when(pruebaService.getPruebas(
      any(Long.class), anyString(), anyString(), any(PageMetadata.class), anyString()))
    .thenReturn(l);

    mockMvc.perform(get("/getPruebas/dummyDNI/23/1234/0/0/1/17"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.metadata.pageSize", is(1)))
    .andExpect(jsonPath("$.metadata.pageNumber", is(17)))
    .andExpect(jsonPath("$.metadata.totalPages", is(20)))
    .andExpect(jsonPath("$.data[0].idPrueba", is(pr.getIdPrueba())))
    .andExpect(jsonPath("$.data[0].tipoPrueba", is("AnalisisSangre")))
    .andExpect(jsonPath("$.data[0].fechaRealizacion", is(
        pr.getFechaRealizacion().toInstant().toEpochMilli())));
  }

  @Test
  public void getPruebas_whenCallingWithDoctorAndPageInRangeAndPCR_shouldReturnPage() throws Exception {
    PruebaResponse pr = new PruebaResponse(
        1, Timestamp.from(Instant.now()), "PCR", "dummyTrabLabNombre");

    List<PruebaResponse> l = List.of(pr);
   
    when(pruebaService.getPagesGetPruebas(
      any(Long.class), anyString(), anyString(), any(Long.class), anyString()))
    .thenReturn((long)20);

    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDoctorDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
    when(pruebaDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(true);

    when(pruebaService.getPruebas(
      any(Long.class), anyString(), anyString(), any(PageMetadata.class), anyString()))
    .thenReturn(l);

    mockMvc.perform(get("/getPruebas/dummyDNI/23/1234/0/0/1/17"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.metadata.pageSize", is(1)))
    .andExpect(jsonPath("$.metadata.pageNumber", is(17)))
    .andExpect(jsonPath("$.metadata.totalPages", is(20)))
    .andExpect(jsonPath("$.data[0].idPrueba", is(pr.getIdPrueba())))
    .andExpect(jsonPath("$.data[0].tipoPrueba", is("PCR")))
    .andExpect(jsonPath("$.data[0].fechaRealizacion", is(
        pr.getFechaRealizacion().toInstant().toEpochMilli())));
  }
}
