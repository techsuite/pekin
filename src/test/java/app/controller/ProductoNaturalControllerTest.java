package app.controller;

import app.dao.ProductoNaturalDAO;
import app.model.Consulta;
import app.model.User;
import app.model.response.ConsultaDetallesResponse;
import app.model.response.PageMetadata;
import app.model.response.ProductoNaturalResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductoNaturalController.class)
public class ProductoNaturalControllerTest {
    @MockBean
    private ProductoNaturalDAO productoNaturalDAO;
    @Autowired
    private MockMvc mockMvc;


    //Todo ok
    @Test
    public void getProductoNatural_whenCallingWell() throws Exception {
        ProductoNaturalResponse resp = new ProductoNaturalResponse(1,"dummyNombre","dummyContraindicaciones","dummyPosologia","dummyProspecto","dummyComposicion", "dummyUrl");
        when(productoNaturalDAO.getProductoNatural(
                anyString(), any(PageMetadata.class)))
                .thenReturn(List.of(resp));
        when(productoNaturalDAO.getNumPages(
                anyString(), any(Long.class)))
                .thenReturn((long)10);

        when(productoNaturalDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(true);
        mockMvc.perform(get("/getInfoProductoNatural?nombre=a&pageSize=1&pageNumber=5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data[0].idProductoNatural", is(resp.getIdProductoNatural())))
                .andExpect(jsonPath("$.data[0].nombre", is(resp.getNombre())))
                .andExpect(jsonPath("$.data[0].contraindicaciones", is(resp.getContraindicaciones())))
                .andExpect(jsonPath("$.data[0].posologia", is(resp.getPosologia())))
                .andExpect(jsonPath("$.data[0].prospecto", is(resp.getProspecto())))
                .andExpect(jsonPath("$.data[0].composicion", is(resp.getComposicion())))
                .andExpect(jsonPath("$.data[0].url", is(resp.getUrl())));

    }

    @Test
    public void getProductoNatural_whenCalledWithPageNummberOutOfRange_shouldReturnBadRequest() throws Exception{
        when(productoNaturalDAO.getNumPages(
                anyString(), any(Long.class)))
                .thenReturn((long)10);
        when(productoNaturalDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

        mockMvc.perform(get("/getInfoProductoNatural?nombre=a&pageSize=1&pageNumber=20"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getProductoNatural_whenCallingWithNonExistingNameInProductoNatural_shouldReturnNoContent() throws Exception {
        when(productoNaturalDAO.getNumPages(
                anyString(), any(Long.class)))
                .thenReturn((long)0);

        mockMvc.perform(get("/getInfoProductoNatural?nombre=a&pageSize=1&pageNumber=1"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void getProductoNatural_CalledWithInvalidPageSize() throws Exception{

        mockMvc.perform(get("/getInfoProductoNatural?nombre=a&pageSize=0&pageNumber=20"))
                .andExpect(status().isBadRequest());
    }
}
