package app.controller;

import app.dao.TokenizacionDAO;
import app.dao.PacienteDAO;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import app.model.response.PacienteResponse;
import app.model.response.PageMetadata;
import app.service.UserService;
import app.service.PacienteService;
import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest(PacienteController.class)
public class PacienteControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private PacienteDAO pacienteDAO;
  @MockBean
  private TokenizacionDAO tokDAO;
  @MockBean
  private UserService userService;  
  @MockBean
  private PacienteService pacienteService;

  @Test
  public void getPacientes_whenCallingWithBothFilters_shouldReturnBadRequest() throws Exception{
    mockMvc.perform(get("/getPacientes/dummyDNI/0/dummyDNIFiltro/dummyNombreFiltro/0/0"))
    .andExpect(status().isBadRequest());
  }

  @Test
  public void getPacientes_whenCallingWithNonExistingTokenInTokenizacion_shouldReturnNoContent() throws Exception {
    when(tokDAO.getDNI(any(Integer.class))).thenReturn(null);

    mockMvc.perform(get("/getPacientes/dummyDNI/0/dummyNombreFiltro/0/0/0"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getPacientes_whenCallingWithWrongRoleDNI_shouldReturnBadRequest() throws Exception {
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of());

    mockMvc.perform(get("/getPacientes/dummyDNI/0/0/dummyDNIFiltro/0/0"))
    .andExpect(status().isBadRequest());
  }

  @Test
  public void getPacientes_whenCallingWithNoPermissionObserver_shouldReturnUnauthorized() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(userService.getRoles(eq("dummyDNI"))).thenReturn(List.of("Doctor"));
    when(userService.getRoles(eq("dummyObserverDNI"))).thenReturn(List.of());

    mockMvc.perform(get("/getPacientes/dummyDNI/0/0/0/0/0"))
    .andExpect(status().is(401)); //Unathorized
  }

  @Test
  public void getPacientes_whenCallingWithDNIWithoutPaciente_shouldReturnNoContent() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));

    when(pacienteService.getPagesGetPacientes(anyString(), anyString(), anyString(), anyString(),
      any(Long.class), anyString())).thenReturn((long)0);

    mockMvc.perform(get("/getPacientes/dummyDNI/23/0/0/1/30"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getPacientes_whenCallingWithPageOutOfRange_shouldReturnBadRequest() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
      
    when(pacienteService.getPagesGetPacientes(anyString(), anyString(), anyString(), anyString(),
      any(Long.class), anyString())).thenReturn((long)20);

    when(pacienteDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

    mockMvc.perform(get("/getPacientes/dummyDNI/23/0/0/1/30"))
    .andExpect(status().isBadRequest());
  }

  @Test
  public void getPacientes_whenCallingWithDoctorAndPageInRange_shouldReturnPage() throws Exception {
    PacienteResponse pr = new PacienteResponse("Hombre", "dummyNombre", "dummyDNI", 28);
    List<PacienteResponse> l = List.of(pr);
   
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDoctorDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));

    when(pacienteService.getPagesGetPacientes(anyString(), anyString(), anyString(), anyString(),
      any(Long.class), anyString())).thenReturn((long)20);

    when(pacienteDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(true);

    when(pacienteService.getPacientes(anyString(), anyString(), anyString(), anyString(), 
      any(PageMetadata.class), anyString()))
    .thenReturn(l);

    mockMvc.perform(get("/getPacientes/dummyDNI/23/0/0/1/17"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.metadata.pageSize", is(1)))
    .andExpect(jsonPath("$.metadata.pageNumber", is(17)))
    .andExpect(jsonPath("$.metadata.totalPages", is(20)))
    .andExpect(jsonPath("$.data[0].genero", is(pr.getGenero())))
    .andExpect(jsonPath("$.data[0].nombre", is(pr.getNombre())))
    .andExpect(jsonPath("$.data[0].dni", is(pr.getDni())))
    .andExpect(jsonPath("$.data[0].edad", is(pr.getEdad())));
  }

  @Test
  public void getPacientes_whenCallingWithTrabLabAndPageInRange_shouldReturnPage() throws Exception {
    PacienteResponse pr = new PacienteResponse("Hombre", "dummyNombre", "dummyDNI", 38);
    List<PacienteResponse> l = List.of(pr);
   
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyTrabLabDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("TrabajadorLaboratorio"));
    
    when(pacienteService.getPagesGetPacientes(anyString(), anyString(), anyString(), anyString(),
      any(Long.class), anyString())).thenReturn((long)20);

    when(pacienteDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(true);

    when(pacienteService.getPacientes(anyString(), anyString(), anyString(), anyString(), 
      any(PageMetadata.class), anyString()))
    .thenReturn(l);

    mockMvc.perform(get("/getPacientes/dummyDNI/23/0/0/1/17"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.metadata.pageSize", is(1)))
    .andExpect(jsonPath("$.metadata.pageNumber", is(17)))
    .andExpect(jsonPath("$.metadata.totalPages", is(20)))
    .andExpect(jsonPath("$.data[0].genero", is(pr.getGenero())))
    .andExpect(jsonPath("$.data[0].nombre", is(pr.getNombre())))
    .andExpect(jsonPath("$.data[0].dni", is(pr.getDni())))
    .andExpect(jsonPath("$.data[0].edad", is(pr.getEdad())));
  }

  @Test
  public void getAllPacientes_whenCallingWithBothFilters_shouldReturnBadRequest() throws Exception{
    mockMvc.perform(get("/getAllPacientes/0/dummyDNIFiltro/dummyNombreFiltro/0/0"))
    .andExpect(status().isBadRequest());
  }

  @Test
  public void getAllPacientes_whenCallingWithNonExistingTokenInTokenizacion_shouldReturnNoContent() throws Exception {
    when(tokDAO.getDNI(any(Integer.class))).thenReturn(null);

    mockMvc.perform(get("/getAllPacientes/0/dummyNombreFiltro/0/0/0"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getAllPacientes_whenCallingWithNoPermissionObserver_shouldReturnUnauthorized() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of());

    mockMvc.perform(get("/getAllPacientes/0/0/dummyDNIFiltro/0/0"))
    .andExpect(status().is(401)); //Unathorized
  }

  @Test
  public void getAllPacientes_whenQueryGivesNoRows_shouldReturnNoContent() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
  
    when(pacienteService.getPagesGetAllPacientes(anyString(), anyString(), any(Long.class)))
    .thenReturn((long)0);

    mockMvc.perform(get("/getAllPacientes/23/0/0/1/30"))
    .andExpect(status().isNoContent());
  }

  @Test
  public void getAllPacientes_whenCallingWithPageOutOfRange_shouldReturnBadRequest() throws Exception{
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyObserverDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));
      
    when(pacienteService.getPagesGetAllPacientes(anyString(), anyString(), any(Long.class)))
    .thenReturn((long)20);

    when(pacienteDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(false);

    mockMvc.perform(get("/getAllPacientes/23/0/0/1/30"))
    .andExpect(status().isBadRequest());
  }
  
  @Test
  public void getAllPacientes_whenCallingWithDoctorObserverAndPageInRange_shouldReturnPage() throws Exception {
    PacienteResponse pr = new PacienteResponse("Hombre", "dummyNombre", "dummyDNI", 17);
    List<PacienteResponse> l = List.of(pr);
   
    when(tokDAO.getDNI(any(Integer.class))).thenReturn("dummyDoctorDNI");
    when(userService.getRoles(anyString())).thenReturn(List.of("Doctor"));

    when(pacienteService.getPagesGetAllPacientes(anyString(), anyString(), any(Long.class)))
    .thenReturn((long)20);

    when(pacienteDAO.isNumPageInRange(any(PageMetadata.class))).thenReturn(true);

    when(pacienteService.getAllPacientes(anyString(), anyString(), any(PageMetadata.class)))
    .thenReturn(l);

    mockMvc.perform(get("/getAllPacientes/23/0/0/1/17"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.metadata.pageSize", is(1)))
    .andExpect(jsonPath("$.metadata.pageNumber", is(17)))
    .andExpect(jsonPath("$.metadata.totalPages", is(20)))
    .andExpect(jsonPath("$.data[0].genero", is(pr.getGenero())))
    .andExpect(jsonPath("$.data[0].nombre", is(pr.getNombre())))
    .andExpect(jsonPath("$.data[0].dni", is(pr.getDni())))
    .andExpect(jsonPath("$.data[0].edad", is(pr.getEdad())));
  }
}
