package app.controller;

import app.dao.TokenizacionDAO;
import app.dao.UserDAO;
import app.model.Otp;
import app.model.User;
import app.mock.UserMocks;
import app.model.request.UserATokenRequest;
import app.model.response.UserAToken;
import app.model.request.UserOTPRequest;
import app.model.request.UserRequest;
import mocks.UserATokenMocks;
import mocks.UserOTPMocks;
import app.model.response.Email;
import app.service.EmailService;
import app.dao.OtpDAO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.javamail.JavaMailSender;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.hamcrest.Matchers.is;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.json.JSONObject;
import org.springframework.http.MediaType;

@RunWith(SpringRunner.class)
@WebMvcTest(RegisterController.class)

public class RegisterControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OtpDAO otpDAO;
    @MockBean
    private UserDAO userDAO;
    @MockBean
    private EmailService emailService;
    @MockBean
    private JavaMailSender envioCorreo;
    @MockBean
    private TokenizacionDAO tokenizacionDAO;

    @Test
    public void generateAuthToken_whenCallingWithoutAllBody() throws Exception{
        UserOTPRequest request = UserOTPMocks.filledRequest();
        request.setOtp(null);
        mockMvc
                .perform(post("/register/generateAuthToken")

                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());

        request.setDni(null);
        mockMvc
                .perform(post("/register/generateAuthToken")

                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void generateAuthToken_whenCallingWithCorrectOTP() throws Exception {
        Timestamp ts = new Timestamp(System.currentTimeMillis()+40000);
        Otp otp = new Otp(1,"07939829V",12345,ts,"tipo1");
        UserOTPRequest request = UserOTPMocks.filledRequest();
        when(otpDAO.getOtp(anyString())).thenReturn(otp);
        mockMvc
                .perform(post("/register/generateAuthToken")

                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(1)));
    }

    @Test
    public void generateAuthToken_whenCallingWithInorrectOTP() throws Exception {
        Timestamp ts = new Timestamp(System.currentTimeMillis()+40000);
        Otp otp = new Otp(1,"07939829V",12346,ts,"tipo1");
        UserOTPRequest request = UserOTPMocks.filledRequest();
        when(otpDAO.getOtp(anyString())).thenReturn(otp);
        mockMvc
                .perform(post("/register/generateAuthToken")

                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void generateAuthToken_whenCallingWithExpiredOTP() throws Exception {
        Timestamp ts = new Timestamp(System.currentTimeMillis()-10000);
        Otp otp = new Otp(1,"07939829V",12345,ts,"tipo1");
        UserOTPRequest request = UserOTPMocks.filledRequest();
        when(otpDAO.getOtp(anyString())).thenReturn(otp);
        mockMvc
                .perform(post("/register/generateAuthToken")

                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void setPassword_whenCallingWithoutAllBody() throws Exception{
        UserATokenRequest request = UserATokenMocks.filledRequest();
        request.setPassword(null);

        mockMvc
        .perform(post("/register/setPassword")

                .content(new JSONObject(request).toString())
                .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest());

        request.setDni(null);

        mockMvc
        .perform(post("/register/setPassword")

                .content(new JSONObject(request).toString())
                .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest());
    }

    @Test
    public void setPassword_whenCallingWithCorrectAuthToken() throws Exception {
        Timestamp ts = new Timestamp(System.currentTimeMillis()+40000);
        Otp otp = new Otp(1,"07939829V",12345,ts,"tipo1");
        UserATokenRequest request = UserATokenMocks.filledRequest();
        //User user = new User("07939829V", "contrasena1", "nombre1", "apellidos1", "genero1", "correo1", 34);
        when(otpDAO.getOtp(anyString())).thenReturn(otp);
        //when(userDAO.getUser(anyString())).thenReturn(user);
        mockMvc
                .perform(post("/register/setPassword")

                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        verify(userDAO).setPassword(anyString(), anyString());
    }

    @Test
    public void setPassword_whenCallingWithEmptyPassword() throws Exception {
        Timestamp ts = new Timestamp(System.currentTimeMillis()+40000);
        Otp otp = new Otp(1,"07939829V",12346,ts,"tipo1");
        UserATokenRequest request = UserATokenMocks.filledRequest();
        request.setPassword("");

        when(otpDAO.getOtp(anyString())).thenReturn(otp);
        mockMvc
                .perform(post("/register/setPassword")

                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void setPassword_whenCallingWithIncorrectAuthToken() throws Exception {
        Timestamp ts = new Timestamp(System.currentTimeMillis()+40000);
        Otp otp = new Otp(1,"07939829V",12346,ts,"tipo1");
        UserATokenRequest request = UserATokenMocks.filledRequest();
        when(otpDAO.getOtp(anyString())).thenReturn(otp);
        mockMvc
                .perform(post("/register/setPassword")

                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void setPassword_whenCallingWithExpiredAuthToken() throws Exception {
        Timestamp ts = new Timestamp(System.currentTimeMillis()-40000);
        Otp otp = new Otp(1,"07939829V",12345,ts,"tipo1");
        UserATokenRequest request = UserATokenMocks.filledRequest();
        when(otpDAO.getOtp(anyString())).thenReturn(otp);
        mockMvc
                .perform(post("/register/setPassword")

                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());
    }

    

    //Prueba get recibe dni devuelve correo censurado. 

    //Prueba 1: todo ok
    @Test
    public void exists_whenCallingWithExistingUser() throws Exception {
      User u = new User();
      u.setDNI("123A"); //Me invento un dni
      Email email= new Email("correo_elec@123");
      u.setCorreo_electronico("correo_elec@123");
      u.setContrasena("");
      when(userDAO.getUser(anyString())).thenReturn(u); //Devuelve usuario

      mockMvc.perform(get("/register/exists/123A"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.correo_electronico", is(email.Censurado()))); //Compruebo que me devueve correo censurado
    }
    //Prueba 2: dni no coincide. Devuelve noContent
    @Test
    public void exists_whenCallingWithNonExistingUser() throws Exception {
      when(userDAO.getUser(anyString())).thenReturn(null);
      mockMvc.perform(get("/register/exists/123A")) //Aqui el dni no está
      .andExpect(status().isNoContent()); //Error 204
    }

    @Test
    public void exists_whenCallingWithAlreadyRegisteredUser() throws Exception {
      User u = new User();
      u.setContrasena("dummyPass");
      when(userDAO.getUser(anyString())).thenReturn(u);
      mockMvc.perform(get("/register/exists/123A"))
      .andExpect(status().isNoContent()); //Error 204
    }

    //Prueba post: recibo dni y correo. Creo clave aleatoria y lo envio a un correo dado

    //Prueba 1: Todo ok
    @Test
    public void nuevoRegistro_whenCalled_shouldSendAndEmailWithRandomOtpCorrect()
    throws Exception {
    UserRequest request = UserMocks.filledRequest();
    User u = new User();
    u.setDNI(request.getDni());
    u.setContrasena("");
    u.setCorreo_electronico(request.getCorreo_electronico());
when(userDAO.getUser(anyString())).thenReturn(u);
//No se ha intentado registrar antes
when(otpDAO.getOtp(anyString())).thenReturn(null);

    mockMvc
    .perform(
        post("/register/generateOTP")
        .content(new JSONObject(request).toString())
        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk());
        //verify(envioCorreo).createMimeMessage();

verify(otpDAO).insertOtp(anyString(), anyInt(), any(), anyString());
Otp otp = new Otp();
otp.setIdOTP(12345);
//Ya se ha intentado registrar antes
when(otpDAO.getOtp(anyString())).thenReturn(otp);

    mockMvc
    .perform(
        post("/register/generateOTP")
        .content(new JSONObject(request).toString())
        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk());

verify(otpDAO).updateOtp(any(Integer.class), anyInt(), any(), anyString());

    }
    @Test
    //Prueba 2: correo no existe. badRequest()
    public void nuevoRegistro_WrongEmail()
        throws Exception {
        UserRequest request = UserMocks.filledRequest();
        request.setCorreo_electronico(null);
        
        mockMvc
        .perform(
            post("/register/generateOTP")
            .content(new JSONObject(request).toString())
            .contentType(MediaType.APPLICATION_JSON)
    )
    .andExpect(status().isBadRequest());
                
        }

    @Test
    //Prueba 3: dni no existe. Me invento nombres de aributos. en vez de dni, poner otro nombre
    public void nuevoRegistro_WrongDni()
    throws Exception {
    UserRequest request = UserMocks.filledRequest();
    request.setDni(null);
    mockMvc
    .perform(
        post("/register/generateOTP")
        .content(new JSONObject(request).toString())
        .contentType(MediaType.APPLICATION_JSON)
)
.andExpect(status().isBadRequest());
}
        @Test
        public void generateOTP_whenCallingWithUserAlreadyRegistered_shouldReturnBadRequest() throws Exception{
                UserRequest request = UserMocks.filledRequest();
                User u = new User();
                u.setDNI(request.getDni());
                u.setContrasena("dummyPassword");
                u.setCorreo_electronico(request.getCorreo_electronico());
                when(userDAO.getUser(anyString())).thenReturn(u);
                mockMvc
                .perform(
                        post("/register/generateOTP")
                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());

        }

        @Test
        public void generateOTP_whenCallingWithNonMatchingEmail_shouldReturnBadRequest() throws Exception{
                UserRequest request = UserMocks.filledRequest();
                User u = new User();
                u.setDNI(request.getDni());
                u.setContrasena("");
                u.setCorreo_electronico("dummy"+request.getCorreo_electronico());
                when(userDAO.getUser(anyString())).thenReturn(u);
                mockMvc
                .perform(
                        post("/register/generateOTP")
                        .content(new JSONObject(request).toString())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest());

        }
}

