package app.service;

import app.dao.ConsultaDAO;
import app.model.response.ConsultaResponse;
import app.model.response.PageMetadata;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;


import java.sql.Timestamp;
import java.time.Instant;
import java.sql.SQLException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsultaServiceTest {
    @MockBean
    ConsultaDAO consultaDAO;
    @Autowired
    ConsultaService consultaService;

    @Test
    public void getConsultas_whenNoFilter() throws SQLException{
        ConsultaResponse cr1 = new ConsultaResponse(
            1, Timestamp.from(Instant.now()), "dummyDoctorNombre");
        ConsultaResponse cr2 = new ConsultaResponse(
            2, Timestamp.from(Instant.now()), "dummyDoctorNombre");

        List<ConsultaResponse> l = List.of(cr1, cr2);

        when(consultaDAO.getPage(
            eq(ConsultaResponse.class), anyString(), any(PageMetadata.class), anyString()))
        .thenReturn(l);

        List<ConsultaResponse> lResultado = consultaService.getConsultas(
            (long)0, "0", new PageMetadata(), "dummyDNI");

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getFechaRealizacion(), lResultado.get(i).getFechaRealizacion());
            Assert.assertEquals(l.get(i).getIdConsulta(), lResultado.get(i).getIdConsulta());
            Assert.assertEquals(l.get(i).getDoctorNombre(), lResultado.get(i).getDoctorNombre());

        }
    }

    @Test
    public void getConsultas_whenFechaFilter() throws SQLException{
        ConsultaResponse cr1 = new ConsultaResponse(
            1, Timestamp.from(Instant.now()), "dummyDoctorNombre");
        ConsultaResponse cr2 = new ConsultaResponse(
            2, Timestamp.from(Instant.now()), "dummyDoctorNombre");

        List<ConsultaResponse> l = List.of(cr1, cr2);

        when(consultaDAO.getPage(
            eq(ConsultaResponse.class), anyString(), 
            any(PageMetadata.class), anyString(), any(Long.class)))
        .thenReturn(l);

        List<ConsultaResponse> lResultado = consultaService.getConsultas(
            (long)123456789, "0", new PageMetadata(), "dummyDNI");
            
        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getFechaRealizacion(), lResultado.get(i).getFechaRealizacion());
            Assert.assertEquals(l.get(i).getIdConsulta(), lResultado.get(i).getIdConsulta());
            Assert.assertEquals(l.get(i).getDoctorNombre(), lResultado.get(i).getDoctorNombre());
        }
    }

    @Test
    public void getConsultas_whenDoctorDNIFilter() throws SQLException{
        ConsultaResponse cr1 = new ConsultaResponse(
            1, Timestamp.from(Instant.now()), "dummyDoctorNombre");

        when(consultaDAO.getPage(
            eq(ConsultaResponse.class), anyString(), 
            any(PageMetadata.class), anyString(), anyString()))
        .thenReturn(List.of(cr1));

        List<ConsultaResponse> lResultado = consultaService.getConsultas(
            (long)0, "dummyDoctorDNI", new PageMetadata(), "dummyDNI");
            
        Assert.assertEquals(1, lResultado.size());
    
        Assert.assertEquals(cr1.getFechaRealizacion(), lResultado.get(0).getFechaRealizacion());
        Assert.assertEquals(cr1.getIdConsulta(), lResultado.get(0).getIdConsulta());
        Assert.assertEquals(cr1.getDoctorNombre(), lResultado.get(0).getDoctorNombre());
    }

    @Test
    public void getPages_whenNoFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(consultaDAO.getNumPages(anyString(), any(Long.class), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            consultaService.getPagesGetConsultas(
                (long)0, "0", (long)1, "dummyPacienteDNI")
        );
    }

    @Test
    public void getPages_whenFechaFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(consultaDAO.getNumPages(anyString(), any(Long.class), anyString(), any(Long.class)))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            consultaService.getPagesGetConsultas(
                (long)123456789, "0", (long)1, "dummyPacienteDNI")
        );
    }

    @Test
    public void getPages_whenDoctorDNIFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(consultaDAO.getNumPages(anyString(), any(Long.class), anyString(), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            consultaService.getPagesGetConsultas(
                (long)0, "dummyDoctorNombreFiltro", (long)1, "dummyPacienteDNI")
        );
    }
}
