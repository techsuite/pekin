package app.service;

import app.dao.PacienteDAO;
import app.model.response.PacienteResponse;
import app.model.response.PageMetadata;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;

import java.sql.SQLException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;


@RunWith(SpringRunner.class)
@SpringBootTest
public class PacienteServiceTest {
    @MockBean
    PacienteDAO pacienteDAO;
    @Autowired
    PacienteService pacienteService;

    @Test
    public void getPacientes_whenNoFilter() throws SQLException{
        PacienteResponse pr1 = new PacienteResponse("Hombre", "dummyNombre", "dummyDNI", 20);
        PacienteResponse pr2 = new PacienteResponse("Mujer", "dummyNombre2", "dummyDNI2", 20);
        
        List<PacienteResponse> l = List.of(pr1, pr2);

        when(pacienteDAO.getPage(
            eq(PacienteResponse.class), anyString(), any(PageMetadata.class), anyString()))
        .thenReturn(l);

        List<PacienteResponse> lResultado = pacienteService.getPacientes(
            "dummyTabla", "dummyRol", "0", "0", new PageMetadata(), "dummyDocTrabLabDNI");

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getDni(), lResultado.get(i).getDni());
            Assert.assertEquals(l.get(i).getGenero(), lResultado.get(i).getGenero());
            Assert.assertEquals(l.get(i).getNombre(), lResultado.get(i).getNombre());
            Assert.assertEquals(l.get(i).getEdad(), lResultado.get(i).getEdad());
        }
    }

    @Test
    public void getPacientes_whenDNIFilter() throws SQLException{
        PacienteResponse pr = new PacienteResponse("Hombre", "dummyNombre", "dummyDNI", 20);
        
        List<PacienteResponse> l = List.of(pr);

        when(pacienteDAO.getPage(
            eq(PacienteResponse.class), anyString(), any(PageMetadata.class), 
                anyString(), anyString()))
        .thenReturn(l);

        List<PacienteResponse> lResultado = pacienteService.getPacientes(
            "dummyTabla", "dummyRol", "dummyDNI", "0", new PageMetadata(), "dummyDocTrabLabDNI");

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getDni(), lResultado.get(i).getDni());
            Assert.assertEquals(l.get(i).getGenero(), lResultado.get(i).getGenero());
            Assert.assertEquals(l.get(i).getNombre(), lResultado.get(i).getNombre());
            Assert.assertEquals(l.get(i).getEdad(), lResultado.get(i).getEdad());
        }
    }

    @Test
    public void getPacientes_whenNameFilter() throws SQLException{
        PacienteResponse pr1 = new PacienteResponse("Hombre", "dummyNombre", "dummyDNI", 20);
        PacienteResponse pr2 = new PacienteResponse("Mujer", "dummyNombre", "dummyDNI2", 20);
        
        List<PacienteResponse> l = List.of(pr1, pr2);

        when(pacienteDAO.getPage(
            eq(PacienteResponse.class), anyString(), any(PageMetadata.class),
                anyString()))
        .thenReturn(l);

        List<PacienteResponse> lResultado = pacienteService.getPacientes(
            "dummyTabla", "dummyRol", "0", "dummyNombre", new PageMetadata(), "dummyDocTrabLabDNI");

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getDni(), lResultado.get(i).getDni());
            Assert.assertEquals(l.get(i).getGenero(), lResultado.get(i).getGenero());
            Assert.assertEquals(l.get(i).getNombre(), lResultado.get(i).getNombre());
            Assert.assertEquals(l.get(i).getEdad(), lResultado.get(i).getEdad());
        }
    }

    @Test
    public void getPagesGetPacientes_whenNoFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pacienteDAO.getNumPages(anyString(), any(Long.class), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pacienteService.getPagesGetPacientes(
                "dummyTabla", "dummyRol", "0", "0", (long)1, "dummyDocTrabLabDNI")
        );
    }

    @Test
    public void getPagesGetPacientes_whenDNIFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pacienteDAO.getNumPages(anyString(), any(Long.class), anyString(), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pacienteService.getPagesGetPacientes(
                "dummyTabla", "dummyRol", "dummyDNI", "0", (long)1, "dummyDocTrabLabDNI")
        );
    }

    @Test
    public void getPagesGetPacientes_whenNameFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pacienteDAO.getNumPages(anyString(), any(Long.class), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pacienteService.getPagesGetPacientes(
                "dummyTabla", "dummyRol", "0", "dummyNombre", (long)1, "dummyDocTrabLabDNI")
        );
    }

    @Test
    public void getAllPacientes_whenNoFilter() throws SQLException{
        PacienteResponse pr1 = new PacienteResponse("Hombre", "dummyNombre", "dummyDNI", 20);
        PacienteResponse pr2 = new PacienteResponse("Mujer", "dummyNombre2", "dummyDNI2", 21);
        
        List<PacienteResponse> l = List.of(pr1, pr2);

        when(pacienteDAO.getPage(
            eq(PacienteResponse.class), anyString(), any(PageMetadata.class)))
        .thenReturn(l);

        List<PacienteResponse> lResultado = pacienteService.getAllPacientes(
            "0", "0", new PageMetadata());

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getDni(), lResultado.get(i).getDni());
            Assert.assertEquals(l.get(i).getGenero(), lResultado.get(i).getGenero());
            Assert.assertEquals(l.get(i).getNombre(), lResultado.get(i).getNombre());
            Assert.assertEquals(l.get(i).getEdad(), lResultado.get(i).getEdad());
        }
    }

    @Test
    public void getAllPacientes_whenDNIFilter() throws SQLException{
        PacienteResponse pr = new PacienteResponse("Hombre", "dummyNombre", "dummyDNI", 27);
        
        List<PacienteResponse> l = List.of(pr);

        when(pacienteDAO.getPage(
            eq(PacienteResponse.class), anyString(), any(PageMetadata.class), anyString()))
        .thenReturn(l);

        List<PacienteResponse> lResultado = pacienteService.getAllPacientes(
            "dummyDNI", "0", new PageMetadata());

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getDni(), lResultado.get(i).getDni());
            Assert.assertEquals(l.get(i).getGenero(), lResultado.get(i).getGenero());
            Assert.assertEquals(l.get(i).getNombre(), lResultado.get(i).getNombre());
            Assert.assertEquals(l.get(i).getEdad(), lResultado.get(i).getEdad());
        }
    }

    @Test
    public void getAllPacientes_whenNameFilter() throws SQLException{
        PacienteResponse pr1 = new PacienteResponse("Hombre", "dummyNombre", "dummyDNI", 27);
        PacienteResponse pr2 = new PacienteResponse("Mujer", "dummyNombre", "dummyDNI2", 48);
        
        List<PacienteResponse> l = List.of(pr1, pr2);

        when(pacienteDAO.getPage(
            eq(PacienteResponse.class), anyString(), any(PageMetadata.class)))
        .thenReturn(l);

        List<PacienteResponse> lResultado = pacienteService.getAllPacientes(
            "0", "dummyNombre", new PageMetadata());

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getDni(), lResultado.get(i).getDni());
            Assert.assertEquals(l.get(i).getGenero(), lResultado.get(i).getGenero());
            Assert.assertEquals(l.get(i).getNombre(), lResultado.get(i).getNombre());
            Assert.assertEquals(l.get(i).getEdad(), lResultado.get(i).getEdad());
        }
    }

    @Test
    public void getPagesGetAllPacientes_whenNoFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pacienteDAO.getNumPages(anyString(), any(Long.class)))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pacienteService.getPagesGetAllPacientes("0", "0", (long)1)
        );
    }

    @Test
    public void getPagesGetAllPacientes_whenDNIFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pacienteDAO.getNumPages(anyString(), any(Long.class), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pacienteService.getPagesGetAllPacientes("dummyDNI", "0", (long)1)
        );
    }

    @Test
    public void getPagesGetAllPacientes_whenNameFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pacienteDAO.getNumPages(anyString(), any(Long.class)))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pacienteService.getPagesGetAllPacientes("0", "dummyNombre", (long)1)
        );
    }
}
