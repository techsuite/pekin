package app.service;

import app.dao.MedicamentoDAO;
import app.model.response.MedicamentoResponse;
import app.model.response.PageMetadata;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;

import java.sql.SQLException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;


@RunWith(SpringRunner.class)
@WebMvcTest(MedicamentoService.class)
public class MedicamentoServiceTest {
    @MockBean
    MedicamentoDAO medicamentoDAO;
    @Autowired
    MedicamentoService medicamentoService;

    @Test
    public void getMedicamentos_whenNoFilter() throws SQLException{
        List<String> l = List.of("medicamento1", "medicamento2");

        when(medicamentoDAO.getPage(anyString(), any(PageMetadata.class), anyString()))
        .thenReturn(l);

        List<String> lResultado = medicamentoService.getMedicamentos(new PageMetadata(), "dummyDNI");

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i), lResultado.get(i));

        }
    }

    @Test
    public void getPages_whenNoFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(medicamentoDAO.getNumPages(anyString(), any(Long.class), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            medicamentoService.getPagesGetMedicamentos(
                (long)0, "dummyPacienteDNI")
        );
    }

    @Test
    public void getInfoMedicamento() throws Exception {
        MedicamentoResponse medResponse1 = new MedicamentoResponse(1,"med1","contraind1","pos1","prosp1","comp1","url1");
        MedicamentoResponse medResponse2 = new MedicamentoResponse(2,"med2","contraind2","pos2","prosp2","comp2","url2");
        List<MedicamentoResponse> l = List.of(medResponse1, medResponse2);

        when(medicamentoDAO.getPage(eq(MedicamentoResponse.class), anyString(), any(PageMetadata.class), anyString()))
        .thenReturn(l);

        List<MedicamentoResponse> lResultado = medicamentoService.getInfoMedicamento(new PageMetadata(), "med");

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i), lResultado.get(i));

        }
    }
}

