package app.service;

import app.dao.EnfermedadDAO;
import app.model.response.EnfermedadResponse;
import app.model.response.PageMetadata;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;

import java.sql.SQLException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;


@RunWith(SpringRunner.class)
@WebMvcTest(EnfermedadService.class)
public class EnfermedadServiceTest {
    @MockBean
    EnfermedadDAO enfermedadDAO;
    @Autowired
    EnfermedadService enfermedadService;

    @Test
    public void getMedicamentos_whenNoFilter() throws SQLException{
        List<String> l = List.of("enfermedad1", "enfermedad2");

        when(enfermedadDAO.getPage(anyString(), any(PageMetadata.class), anyString()))
        .thenReturn(l);

        List<String> lResultado = enfermedadService.getEnfermedades(new PageMetadata(), "dummyDNI");

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i), lResultado.get(i));

        }
    }

    @Test
    public void getPages_whenNoFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(enfermedadDAO.getNumPages(anyString(), any(Long.class), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            enfermedadService.getPagesGetEnfermedades(
                (long)0, "dummyPacienteDNI")
        );
    }


    @Test
    public void getInfoMedicamento() throws Exception {
        EnfermedadResponse enfResponse1 = new EnfermedadResponse(1,"enf1","sint1","recom1","url1");
        EnfermedadResponse enfResponse2 = new EnfermedadResponse(2,"enf2","sint2","recom2","url2");
        List<EnfermedadResponse> l = List.of(enfResponse1, enfResponse2);

        when(enfermedadDAO.getPage(eq(EnfermedadResponse.class), anyString(), any(PageMetadata.class), anyString()))
        .thenReturn(l);

        List<EnfermedadResponse> lResultado = enfermedadService.getInfoEnfermedad(new PageMetadata(), "enf");

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i), lResultado.get(i));

        }
    }
}