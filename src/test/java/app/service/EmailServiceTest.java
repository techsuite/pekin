package app.service;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import app.factory.SimpleFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceTest {

  @InjectMocks
  private EmailService emailService;

  @Mock
  private JavaMailSender javaMailSender;

  @Mock
  private SimpleFactory simpleFactory;

  @Mock
  private MimeMessageHelper mimeMessageHelper;

  @Test
  public void sendEmail_whenCalled_shouldExecuteSendWithCorrectParams()
    throws Exception {
    when(simpleFactory.mimeMessageHelper(any())).thenReturn(mimeMessageHelper);

    emailService.sendEmail("test@gmail.com", "mensaje", "asunto");

    verify(mimeMessageHelper).setTo("test@gmail.com");
    verify(mimeMessageHelper).setSubject("asunto");
    verify(mimeMessageHelper).setText("mensaje", true);
  }
}
