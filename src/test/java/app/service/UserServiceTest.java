package app.service;

import app.dao.*;
import app.model.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    @MockBean
    PacienteDAO pDAO;
    @MockBean
    DoctorDAO dDAO;
    @MockBean
    ImpostorDAO iDAO;
    @MockBean
    TrabajadorLaboratorioDAO tDAO;
    @Autowired
    UserService userService;

    @Test
    public void getRoles_whenCallingWithNonExistingToken_shouldReturnEmptyList() throws SQLException{
        when(pDAO.getPaciente(anyString())).thenReturn(null);
        when(dDAO.getDoctor(anyString())).thenReturn(null);
        when(iDAO.getImpostor(anyString())).thenReturn(null);
        when(tDAO.getTrabajadorLaboratorio(anyString())).thenReturn(null);

        Assert.assertTrue(userService.getRoles("dummyDNI").isEmpty());
    }

    @Test
    //User with all roles
    public void getRoles_whenCallingWithExistingToken_shouldReturnRolesList() throws SQLException{
        String allRoles[] = {"Paciente", "Doctor", "Impostor", "TrabajadorLaboratorio"};
        
        when(pDAO.getPaciente(anyString())).thenReturn(new Paciente());
        when(dDAO.getDoctor(anyString())).thenReturn(new Doctor());
        when(iDAO.getImpostor(anyString())).thenReturn(new Impostor());
        when(tDAO.getTrabajadorLaboratorio(anyString())).thenReturn(new TrabajadorLaboratorio());

        List<String> roles = userService.getRoles("dummyDNI");
        Assert.assertEquals(allRoles.length, roles.size());
    
        for(String rol : allRoles)
            Assert.assertTrue(roles.contains(rol));
    }

    @Test
    //User without all roles
    public void getRoles_whenCallingWithExistingToken2_shouldReturnRolesList() throws SQLException{
        String allRoles[] = {"Doctor", "Impostor"};
        
        when(pDAO.getPaciente(anyString())).thenReturn(null);
        when(dDAO.getDoctor(anyString())).thenReturn(new Doctor());
        when(iDAO.getImpostor(anyString())).thenReturn(new Impostor());
        when(tDAO.getTrabajadorLaboratorio(anyString())).thenReturn(null);

        List<String> roles = userService.getRoles("dummyDNI");
        Assert.assertEquals(allRoles.length, roles.size());
        
        Assert.assertTrue(roles.contains("Doctor"));
        Assert.assertTrue(roles.contains("Impostor"));
        Assert.assertFalse(roles.contains("Paciente"));
        Assert.assertFalse(roles.contains("TrabajadorLaboratorio"));
    }
   
}
