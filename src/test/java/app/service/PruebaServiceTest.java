package app.service;

import app.dao.PruebaDAO;
import app.dao.PCRDAO;
import app.model.response.PruebaResponse;
import app.model.response.PageMetadata;

import app.model.PCR;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;


import java.sql.Timestamp;
import java.time.Instant;
import java.sql.SQLException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;


@RunWith(SpringRunner.class)
@SpringBootTest
public class PruebaServiceTest {
    @MockBean
    PruebaDAO pruebaDAO;
    @MockBean
    PCRDAO pcrDAO;
    @Autowired
    PruebaService pruebaService;

    @Test
    public void getPruebas_whenNoFilter() throws SQLException{
        PruebaResponse pr1 = new PruebaResponse(
            1, Timestamp.from(Instant.now()), "tipoPCR", "dummyTrabLabNombre");
        
        PruebaResponse pr2 = new PruebaResponse(
            2, Timestamp.from(Instant.now()), 
            "tipoAnalisisSangre", "dummyTrabLabNombre");

        List<PruebaResponse> l = List.of(pr1, pr2);

        when(pruebaDAO.getPage(
            eq(PruebaResponse.class), anyString(), any(PageMetadata.class), anyString()))
        .thenReturn(l);

        when(pcrDAO.getPCR(eq(1))).thenReturn(new PCR());
        when(pcrDAO.getPCR(eq(2))).thenReturn(null);

        List<PruebaResponse> lResultado = pruebaService.getPruebas(
            (long)0, "0", "0", new PageMetadata(), "dummyDNI");

        Assert.assertEquals(l.size(), lResultado.size());
        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getFechaRealizacion(), lResultado.get(i).getFechaRealizacion());
            Assert.assertEquals(l.get(i).getIdPrueba(), lResultado.get(i).getIdPrueba());
            Assert.assertEquals(l.get(i).getTrabLabNombre(), lResultado.get(i).getTrabLabNombre());
        }

        Assert.assertEquals("PCR", lResultado.get(0).getTipoPrueba());
        Assert.assertEquals("AnalisisSangre", lResultado.get(1).getTipoPrueba());
    }

    @Test
    public void getPruebas_whenTrabLabDNIFilter() throws SQLException{
        PruebaResponse pr1 = new PruebaResponse(
            1, Timestamp.from(Instant.now()), "tipoPCR", "dummyTrabLabNombre");

        when(pruebaDAO.getPage(
            eq(PruebaResponse.class), anyString(), any(PageMetadata.class), anyString(), anyString()))
        .thenReturn(List.of(pr1));

        when(pcrDAO.getPCR(eq(1))).thenReturn(new PCR());
        when(pcrDAO.getPCR(eq(2))).thenReturn(null);

        List<PruebaResponse> lResultado = pruebaService.getPruebas(
            (long)0, "dummyTrabLabDNI", "0", new PageMetadata(), "dummyDNI");

        Assert.assertEquals(1, lResultado.size());
      
        Assert.assertEquals(pr1.getFechaRealizacion(), lResultado.get(0).getFechaRealizacion());
        Assert.assertEquals(pr1.getIdPrueba(), lResultado.get(0).getIdPrueba());
        Assert.assertEquals("PCR", lResultado.get(0).getTipoPrueba());
        Assert.assertEquals(pr1.getTrabLabNombre(), lResultado.get(0).getTrabLabNombre());

    }

    @Test
    public void getPruebas_whenFechaFilter() throws SQLException{
        PruebaResponse pr1 = new PruebaResponse(
            1, Timestamp.from(Instant.now()), "tipoPCR", "dummyTrabLabNombre");
        
        PruebaResponse pr2 = new PruebaResponse(
            2, Timestamp.from(Instant.now()), 
            "tipoAnalisisSangre", "dummyTrabLabNombre");

        List<PruebaResponse> l = List.of(pr1, pr2);

        when(pruebaDAO.getPage(
            eq(PruebaResponse.class), anyString(), any(PageMetadata.class), anyString(), any(Long.class)))
        .thenReturn(l);

        when(pcrDAO.getPCR(eq(1))).thenReturn(new PCR());
        when(pcrDAO.getPCR(eq(2))).thenReturn(null);

        List<PruebaResponse> lResultado = pruebaService.getPruebas(
            pr1.getFechaRealizacion().toInstant().toEpochMilli(),
            "0", "0", new PageMetadata(), "dummyDNI");

        Assert.assertEquals(l.size(), lResultado.size());

        for(int i = 0; i < l.size(); i++){
            Assert.assertEquals(l.get(i).getFechaRealizacion(), lResultado.get(i).getFechaRealizacion());
            Assert.assertEquals(l.get(i).getIdPrueba(), lResultado.get(i).getIdPrueba());
            Assert.assertEquals(l.get(i).getTrabLabNombre(), lResultado.get(i).getTrabLabNombre());
        }

        Assert.assertEquals("PCR", lResultado.get(0).getTipoPrueba());
        Assert.assertEquals("AnalisisSangre", lResultado.get(1).getTipoPrueba());
    }

    @Test
    public void getPruebas_whenTipoPruebaFilter() throws SQLException{
        PruebaResponse pr2 = new PruebaResponse(
            2, Timestamp.from(Instant.now()), 
            "AnalisisSangre", "dummyTrabLabNombre");

        when(pruebaDAO.getPage(
            eq(PruebaResponse.class), anyString(), any(PageMetadata.class), anyString()))
        .thenReturn(List.of(pr2));

        List<PruebaResponse> lResultado = pruebaService.getPruebas(
            (long)0, "0", "AnalisisSangre", new PageMetadata(), "dummyDNI");

        Assert.assertEquals(1, lResultado.size());
        
        Assert.assertEquals(pr2.getFechaRealizacion(), lResultado.get(0).getFechaRealizacion());
        Assert.assertEquals(pr2.getIdPrueba(), lResultado.get(0).getIdPrueba());
        Assert.assertEquals("AnalisisSangre", lResultado.get(0).getTipoPrueba());
        Assert.assertEquals(pr2.getTrabLabNombre(), lResultado.get(0).getTrabLabNombre());
    }

    @Test
    public void getPages_whenNoFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pruebaDAO.getNumPages(anyString(), any(Long.class), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pruebaService.getPagesGetPruebas(
                (long)0, "0", "0", (long)1, "dummyPacienteDNI")
        );
    }

    @Test
    public void getPages_whenTrabLabFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pruebaDAO.getNumPages(anyString(), any(Long.class), anyString(), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pruebaService.getPagesGetPruebas(
                (long)0, "dummyTrabLabDNIFilter", "0", (long)1, "dummyPacienteDNI")
        );
    }

    @Test
    public void getPages_whenFechaFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pruebaDAO.getNumPages(anyString(), any(Long.class), anyString(), any(Long.class)))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pruebaService.getPagesGetPruebas(
                (long)123456789, "0", "0", (long)1, "dummyPacienteDNI")
        );
    }

    @Test
    public void getPages_whenTipoPruebaFilter() throws Exception{
        Long numPages = Long.valueOf(20);
        when(pruebaDAO.getNumPages(anyString(), any(Long.class), anyString()))
        .thenReturn(numPages);

        Assert.assertEquals(
            numPages, 
            pruebaService.getPagesGetPruebas(
                (long)0, "0", "dummyTipoPruebaFilter", (long)1, "dummyPacienteDNI")
        );
    }
}
