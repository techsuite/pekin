package mocks;

import app.model.request.UserOTPRequest;

public class UserOTPMocks {
    public static UserOTPRequest filledRequest() {
        return new UserOTPRequest("07939829V", 12345);
    }
}
