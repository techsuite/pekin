package mocks;

import app.model.request.UserATokenRequest;

public class UserATokenMocks {
    public static UserATokenRequest filledRequest() {
        return new UserATokenRequest("07939829V", 12345, "contrasena1");
    }
}
